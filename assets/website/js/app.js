$(window).ready(function(){
    app.init();
    app.signup();
    app.login();
    app.verify();
    app.profile();
    app.poster();
});

var app = {
	init: function(){
    if(page == 'home' || page == 'posters' ){
      $.ajax({
        type: "POST",
        url: base_url + "Users/get_posters_name",
        dataType: "json",
        success: function(result) {
          $( "#suggest_item" ).autocomplete({
              source: result
            });
        },
      });

      $(document).on('submit', '#contact_us_form', function(event){
      event.preventDefault();
            var lname= $('#contact_us_form #lname').val();
            var fname = $('#contact_us_form #fname').val();
            var subject = $('#contact_us_form #subject').val();
            var email = $('#contact_us_form #email').val();
            var content= $('#contact_us_form #content').val();
            var data = {'lname':lname,'fname':fname,'subject':subject,'content':content,'email':email};
            // alert(JSON.stringify(data));
            $.ajax({
              type:'POST',
              dataType:'JSON',
              url:base_url+'users/send_report',
              data:data,
              success:function(data)
              {
                if(data == 1){
                  app.alert_redirection('Success','Report Send thank you!',base_url);
              }else{
                app.alert('Error','Report does not send');
              }
              }
            });
        });
    }

    
	},

  poster: function(){
    $(document).on('submit', '#poster_select', function(event){
      event.preventDefault();
      var download = $('#poster_select #multi_select').val();
      alert(download);
      $('#clicker'+download).click();
    });
  },

  profile: function(){
    $(document).on('submit', '#update_profile_user', function(event){
      event.preventDefault();
            var lname= $('#update_profile_user #lname').val();
            var fname = $('#update_profile_user #fname').val();
            var mname= $('#update_profile_user #mname').val();
            var email = $('#update_profile_user #email').val();
            var number = $('#update_profile_user #number').val();
            var password= $('#update_profile_user #password').val();
            var data = {'lname':lname,'fname':fname,'mname':mname,'email':email,'password':password,'number':number};
            // alert(JSON.stringify(data));
            $.ajax({
              type:'POST',
              dataType:'JSON',
              url:base_url+'users/user_profile',
              data:data,
              success:function(data)
              {
                if(data == 1){
                  app.alert_redirection('Success','Profile Updated',base_url+'profile');
              }else{
                app.alert('Error','Updated error');
              }
              }
            });
        });
  },

  alert: function(title,content){
    $.alert({
              scrollToPreviousElement:false,
              backgroundDismiss: false,
              closeIcon: true,
              closeIconClass: 'fa fa-close',
              titleClass: 'dialogHeader',
              draggable: false,
              animation: 'none',
              backgroundDismissAnimation: 'none',
              columnClass: 'small',
              title: '<div>'+title+'</div>',
              content: '<div class="dialogContent">'+content+'</div>',
              buttons: {
                Ok: {
                  text: 'Ok',
                  btnClass: 'clearButton',
                }
              },
    });
  },

  alert_redirection: function(title,content,redirect){
    $.alert({
              scrollToPreviousElement:false,
              backgroundDismiss: false,
              closeIcon: true,
              closeIconClass: 'fa fa-close',
              titleClass: 'dialogHeader',
              draggable: false,
              animation: 'none',
              backgroundDismissAnimation: 'none',
              columnClass: 'small',
              title: '<div>'+title+'</div>',
              content: '<div class="dialogContent">'+content+'</div>',
              buttons: {
                Ok: {
                  text: 'Ok',
                  btnClass: 'clearButton',
                  action: function () {
                    window.location = redirect;
                  }
                }
              },
    });
  },
  
  signup: function(){
    $(document).on('submit','#sign_up',function(e){
			e.preventDefault();
			var fname = $('#sign_up #fname').val();
			var mname = $('#sign_up #mname').val();
			var lname = $('#sign_up #lname').val();
			var number = $('#sign_up #number').val();
			var email = $('#sign_up #email').val();
			var password = $('#sign_up #password').val();
      var cpassword = $('#sign_up #cpassword').val();
			var verification_key = $('#sign_up #verification_key').val();
			var is_verified = $('#sign_up #is_verified').val();
			var data = {'fname':fname,'mname':mname,'lname':lname,'number':number,'email':email,'password':password,'verification_key':verification_key,'is_verified':is_verified};
			// alert(JSON.stringify(data));

      if(password == cpassword){
        $.ajax({
                  type:'POST',
                  dataType:'JSON',
                  url:base_url+"users/sign_up",
                  data:data,
                  success:function(data)
                  {
                  	if(data == 1){
  	                    app.alert_redirection('Success','User Added',base_url);

                  	}else{
                  		app.alert('Error','There`s something wrong please reload the page');
                  	}
                  }
            	});
      } else{
        app.alert('Error','Password and Confirm Password must match.');
      }

		});
  },
  verify: function(){
    $(document).on('submit','#verify_user',function(e){
      e.preventDefault();
      var verification_key = $('#verify_user #vkey').val();
      var hverification_key = $('#verify_user #hvkey').val();
      var data = {'verification_key':verification_key};
      // alert(JSON.stringify(data));
      if(verification_key != hverification_key){
        app.alert('Error','Wrong verification key');
      }else{
        $.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+'users/verify_user',
                data:data,
                success:function(data)
                {
                  if(data == 1){
                      app.alert_redirection('Success','Account Verified',base_url);
                  }else{
                    app.alert('Error','There`s something wrong');
                  }
                }
            });
      }
            
    });
  },
  login: function(){
    $(document).on('submit','#forgotpassword2',function(e){
      e.preventDefault();
      var email = $('#forgotpassword2 #email').val();
      var data = {'email':email};
      // alert(JSON.stringify(data));
      $.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+'users/users_forget_password',
                data:data,
                success:function(data)
                {
                  if(data == 1){
                      app.alert('Success','SMS SEND');
                }else{
                    app.alert('Error','Wrong email');
                  }
                }
            });
    });
    $(document).on('submit','#login_user',function(e){
      e.preventDefault();
      var email = $('#login_user #email').val();
      var pwd = $('#login_user #password2').val();
      var data = {'email':email,'password':pwd};
      // alert(JSON.stringify(data));
            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"users/login_user",
                data:data,
                success:function(data)
                {
                  if(data == 1){
                      app.alert('Success','Welcome');
                      setTimeout(function(){window.location = base_url},3000)
                  }else if(data == 2){
                      app.alert('Success','Welcome');
                      setTimeout(function(){window.location = base_url+'verification'},3000)
                  }else{
                    app.alert('Wrong Information','Wrong email or password');
                  }
                }
            });
    });
  },

  getFileExtension: function(filename){
    var ext = /^.+\.([^.]+)$/.exec(filename);
    return ext == null ? "" : ext[1];
  },
}
