$(function () {
  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  //--------------
  //- AREA CHART -
  //--------------

  // Get context with jQuery - using jQuery's .get() method.

  var poster_counts = [];
  var date_label = [];


  // var areaChartCanvas = $('#posterChart').get(0).getContext('2d');
  //
  // // This will get the first returned node in the jQuery collection.
  // var areaChart       = new Chart(areaChartCanvas);
  //
  // // console.log(['January', 'February', 'March', 'April', 'May', 'June', 'July']);
  // var areaChartData = {
  //   labels : date_label,
  //   datasets: [
  //     {
  //       label               : 'Posters',
  //       fillColor           : 'rgba(60,141,188,0.9)',
  //       strokeColor         : 'rgba(60,141,188,0.8)',
  //       pointColor          : '#3b8bba',
  //       pointStrokeColor    : 'rgba(60,141,188,1)',
  //       pointHighlightFill  : '#fff',
  //       pointHighlightStroke: 'rgba(60,141,188,1)',
  //       // data                : [65, 59, 80, 81, 56, 55, 40]
  //       data                : poster_counts
  //     },
  // }
  //
  // var areaChartOptions = {
  //   //Boolean - If we should show the scale at all
  //   showScale               : true,
  //   //Boolean - Whether grid lines are shown across the chart
  //   scaleShowGridLines      : false,
  //   //String - Colour of the grid lines
  //   scaleGridLineColor      : 'rgba(0,0,0,.05)',
  //   //Number - Width of the grid lines
  //   scaleGridLineWidth      : 1,
  //   //Boolean - Whether to show horizontal lines (except X axis)
  //   scaleShowHorizontalLines: true,
  //   //Boolean - Whether to show vertical lines (except Y axis)
  //   scaleShowVerticalLines  : true,
  //   //Boolean - Whether the line is curved between points
  //   bezierCurve             : true,
  //   //Number - Tension of the bezier curve between points
  //   bezierCurveTension      : 0.3,
  //   //Boolean - Whether to show a dot for each point
  //   pointDot                : false,
  //   //Number - Radius of each point dot in pixels
  //   pointDotRadius          : 4,
  //   //Number - Pixel width of point dot stroke
  //   pointDotStrokeWidth     : 1,
  //   //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
  //   pointHitDetectionRadius : 20,
  //   //Boolean - Whether to show a stroke for datasets
  //   datasetStroke           : true,
  //   //Number - Pixel width of dataset stroke
  //   datasetStrokeWidth      : 2,
  //   //Boolean - Whether to fill the dataset with a color
  //   datasetFill             : true,
  //   //String - A legend template
  //   legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
  //   //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
  //   maintainAspectRatio     : true,
  //   //Boolean - whether to make the chart responsive to window resizing
  //   responsive              : true
  // }
  var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  $.ajax({
      type: "POST",
      url: base_url + "cms/get_poster_counts",
      data: {},
      dataType: "json",
      success: function(result) {
          $.each(result, function( index, value ) {
            // alert( index + ": " + value );
            // console.log();
            poster_counts.push(parseInt(value.poster_count));
            date_label.push(months[value.monthly - 1] + " " + value.yearly);

            // poster_counts.reverse();
            // date_label.reverse();
          });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
          if (textStatus == 'timeout') {
              $.alert({
                  closeIcon: true,
                  draggable: false,
                  typeAnimated: true,
                  animation: 'none',
                  backgroundDismissAnimation: 'none',
                  closeIconClass: 'fa fa-close',
                  columnClass: 'small',
                  icon: 'fa fa-close',
                  type: 'red',
                  title: 'Failed!',
                  content: 'Connection timeout. Please check your internet connection.'
              });
          } else {
              $.alert({
                  closeIcon: true,
                  draggable: false,
                  typeAnimated: true,
                  animation: 'none',
                  backgroundDismissAnimation: 'none',
                  closeIconClass: 'fa fa-close',
                  columnClass: 'small',
                  icon: 'fa fa-close',
                  type: 'red',
                  title: 'Failed!',
                  content: 'An error occured while sending data to the server. Please try again.'
              });
          }
      }
  });

  setTimeout(function(){
    var ctx = document.getElementById('posterChart');

    // window.chartColors = {
    // 	red: 'rgb(255, 99, 132)',
    // 	orange: 'rgb(255, 159, 64)',
    // 	yellow: 'rgb(255, 205, 86)',
    // 	green: 'rgb(75, 192, 192)',
    // 	blue: 'rgb(54, 162, 235)',
    // 	purple: 'rgb(153, 102, 255)',
    // 	grey: 'rgb(201, 203, 207)'
    // };

    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: date_label,
        datasets: [{
          label: 'Poster Counts',
          data:poster_counts,
          backgroundColor: [
          'rgba(255, 99, 132, 0.2)'
          ],
          borderColor: [
          'rgba(255, 99, 132, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }, 1000);
})
