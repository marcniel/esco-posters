$(window).ready(function(){
    app.init();
    app.login();
    app.category();
    app.subcategory();
    app.usubcategory();
    app.profile();
    app.users();
    app.poster();
    app.report();
});
var app = {
	init: function(){
		var viewport = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		// alert(viewport);
			if(viewport <= '1000'){
				//category
				$('.category_type_button').html('<i class="fa fa-list-alt" aria-hidden="true"></i>');
				$('.subcategory_type_button').html('Sub - <i class="fa fa-list-alt" aria-hidden="true"></i>');
				$('.usubcategory_type_button').html('Under - Sub - <i class="fa fa-list-alt" aria-hidden="true"></i>');

		    // document.body.style.backgroundColor = "yellow";
		    $('.col1').removeClass('col-md-3');
		  	$('.col1').removeClass('col-lg-2');
		  	$('.col1').removeClass('col-xs-3');
		  	$('.col1').removeClass('col-3');

		  	$('.col2').removeClass('col-md-9');
		  	$('.col2').removeClass('col-lg-10');
		  	$('.col2').removeClass('col-xs-9');
		  	$('.col2').removeClass('col-9');

			$('.col1').hide();		  	
			$('.col2').css({'margin':'20px'});
			$('.headnav').show();
			}else{

		  	$('.col1').addClass('col-md-3');
		  	$('.col1').addClass('col-lg-2');
		  	$('.col1').addClass('col-xs-3');
		  	$('.col1').addClass('col-3');

		  	$('.col2').addClass('col-md-9');
		  	$('.col2').addClass('col-lg-10');
		  	$('.col2').addClass('col-xs-9');
		  	$('.col2').addClass('col-9');
		  	  
		  	$('.col1').show();
			$('.headnav').hide();
		  	}
		
	},
	report: function(){
		$(document).on('submit','#send_email',function(e){
			e.preventDefault();
			var subject = $('#send_email #subject').val();
			var email_content = $('#send_email #email_content').val();
			var email = $('#send_email #email').val();
			var data = {'email':email,'email_content':email_content,'subject':subject};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+'cms/send_email',
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Email Sent',base_url+"cms/reports");
                	}else{
                		app.alert('Wrong Information','Wrong email or password');
                	}
                }
          	});
		});
	},
	login: function(){
		$(document).on('submit','#submit_cms_login',function(e){
			e.preventDefault();
			var email = $('#submit_cms_login #email').val();
			var pwd = $('#submit_cms_login #password').val();
			var data = {'email':email,'password':pwd};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:'login_submit',
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert('Success','Welcome');
	                    setTimeout(function(){window.location = base_url+"cms/"},3000)
                	}else{
                		app.alert('Wrong Information','Wrong email or password');
                	}
                }
          	});
		});

		$(document).on('submit','#forgotpassword',function(e){
			e.preventDefault();
			var email = $('#forgotpassword #email').val();
			var data = {'email':email};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+'cms/admin_forget_password',
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert('Success','SMS SEND');
	            	}else{
                		app.alert('Error','Wrong email');
                	}
                }
          	});
		});
	},

	category: function(){
		$(document).on('submit','#add_category',function(e){
			e.preventDefault();
			var category = $('#add_category #category').val();
			var data = {'category':category};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/add_category",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Category Added',base_url+"cms/categories");
	                    
                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});


		$(document).on('submit','#edit_category',function(e){
			e.preventDefault();
			var category = $('#edit_category #category').val();
			var id = $('#edit_category #id').val();
			var data = {'id':id,'category':category};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/edit_category",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Category Updated',base_url+"cms/categories");

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});


		$(document).on('submit','#delcategory',function(e){
			e.preventDefault();
			var id = $('#delcategory #id').val();
			var data = {'id':id};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/delcategory",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Category Deleted',base_url+"cms/categories");

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});
	},


	subcategory: function(){
		$(document).on('submit','#add_sub_category',function(e){
			e.preventDefault();
			var category = $('#add_sub_category #category').val();
			var sub_category = $('#add_sub_category #subcategory').val();
			var data = {'category':category,'sub_category':sub_category};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/add_sub_category",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Sub-category Added',location.reload());

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});


		$(document).on('submit','#editsubcategory',function(e){
			e.preventDefault();
			var category = $('#editsubcategory #category').val();
			var sub_category = $('#editsubcategory #subcategory').val();
			var id = $('#editsubcategory #id').val();
			var data = {'id':id,'category':category,'sub_category':sub_category};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/editsubcategory",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Sub-Category Updated',location.reload());

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});


		$(document).on('submit','#delete_sub_category',function(e){
			e.preventDefault();
			var id = $('#delete_sub_category #id').val();
			var data = {'id':id};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/delete_sub_category",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Sub-Category Deleted',location.reload());

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});
	},

	usubcategory: function(){
		$(document).on('submit','#add_usub_category',function(e){
			e.preventDefault();
			var sub_category = $('#add_usub_category #subcategory').val();
			var usub_category = $('#add_usub_category #usubcategory').val();
			var data = {'sub_category':sub_category,'usub_category':usub_category};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/add_usub_category",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Under Sub-category Added',location.reload());

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});

		$(document).on('submit','#edit_usub_category',function(e){
			e.preventDefault();
			var sub_category = $('#edit_usub_category #subcategory').val();
			var usub_category = $('#edit_usub_category #usubcategory').val();
			var id = $('#edit_usub_category #id').val();
			var data = {'id':id,'sub_category':sub_category,'usub_category':usub_category};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/edit_usub_category",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Under Sub-Category Updated',location.reload());

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});

		$(document).on('submit','#delete_usub_category',function(e){
			e.preventDefault();
			var id = $('#delete_usub_category #id').val();
			var data = {'id':id};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/delete_usub_category",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Under Sub-Category Deleted',location.reload());

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});
	},

	alert: function(title,content){
		$.alert({
							scrollToPreviousElement:false,
							backgroundDismiss: false,
							closeIcon: true,
							closeIconClass: 'fa fa-close',
							titleClass: 'dialogHeader',
							draggable: false,
							animation: 'none',
							backgroundDismissAnimation: 'none',
							columnClass: 'small',
							title: '<div>'+title+'</div>',
							content: '<div class="dialogContent">'+content+'</div>',
							buttons: {
								Ok: {
									text: 'Ok',
									btnClass: 'clearButton',
								}
							},
		});
	},

	alert_redirection: function(title,content,redirect){
		$.alert({
							scrollToPreviousElement:false,
							backgroundDismiss: false,
							closeIcon: true,
							closeIconClass: 'fa fa-close',
							titleClass: 'dialogHeader',
							draggable: false,
							animation: 'none',
							backgroundDismissAnimation: 'none',
							columnClass: 'small',
							title: '<div>'+title+'</div>',
							content: '<div class="dialogContent">'+content+'</div>',
							buttons: {
								Ok: {
									text: 'Ok',
									btnClass: 'clearButton',
									action: function () {
										window.location = redirect;
									}
								}
							},
		});
	},

	profile: function(){
		$(document).on('submit', '#update_profile', function(event){
    	event.preventDefault();
            var lname= $('#update_profile #lname').val();
            var fname = $('#update_profile #fname').val();
            var mname= $('#update_profile #mname').val();
            var email = $('#update_profile #email').val();
            var password= $('#update_profile #password').val();
            var data = {'lname':lname,'fname':fname,'mname':mname,'email':email,'password':password};
            // alert(JSON.stringify(data));
            $.ajax({
              type:'POST',
              dataType:'JSON',
              url:base_url+'cms/admin_profile',
              data:data,
              success:function(data)
              {
              	if(data == 1){
                  app.alert('Success','Profile Updated');
            	}else{
            		app.alert('Error','Updated error');
            	}
              }
            });
        });
	},

	users: function(){
		$(document).on('submit','#add_user',function(e){
			e.preventDefault();
			var fname = $('#add_user #fname').val();
			var mname = $('#add_user #mname').val();
			var lname = $('#add_user #lname').val();
			var number = $('#add_user #number').val();
			var email = $('#add_user #email').val();
			var password = $('#add_user #password').val();
			var verification_key = $('#add_user #verification_key').val();
			var is_verified = $('#add_user #is_verified').val();
			var data = {'fname':fname,'mname':mname,'lname':lname,'number':number,'email':email,'password':password,'verification_key':verification_key,'is_verified':is_verified};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/add_user",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','User Added',base_url+'cms/users');

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});

		$(document).on('submit','#edit_user',function(e){
			e.preventDefault();
			var fname = $('#edit_user #fname').val();
			var mname = $('#edit_user #mname').val();
			var lname = $('#edit_user #lname').val();
			var number = $('#edit_user #number').val();
			var email = $('#edit_user #email').val();
      var old_email = $('#edit_user #old_email').val();
			var password = $('#edit_user #password2').val();
			var id = $('#edit_user #id').val();
			var data = {'fname':fname,'mname':mname,'lname':lname,'number':number,'email':email,'old_email':old_email,'password':password,'id':id};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/edit_user",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','User Successfully Updated',base_url+'cms/users');

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});

		$(document).on('submit','#delete_user',function(e){
			e.preventDefault();
			var id = $('#delete_user #id').val();
			var data = {'id':id};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/delete_user",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','User Successfully Deleted',base_url+'cms/users');

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});
	},
	poster: function(){
		$(document).on('submit','#add_poster',function(e){
			e.preventDefault();
			var poster_file = $('#add_poster #poster_file').val();
			var poster_file_thumb = $('#add_poster #poster_file_thumb').val();
			var poster_by = $('#add_poster #poster_by').val();
			var poster_title = $('#add_poster #poster_title').val();
			var poster_content = $('#add_poster #poster_content').val();
			var category = $('#add_poster #category').val();
			var sub_category = $('#add_poster #sub_category').val();
			var usub_category = $('#add_poster #usub_category').val();
			var is_downloadable = $('#add_poster #is_downloadable').val();
			var posted_date = $('#add_poster #posted_date').val();
			var data = {
				'poster_file':poster_file,
				'poster_file_thumb':poster_file_thumb,
				'poster_by':poster_by,
				'poster_name':poster_title,
				'poster_content':poster_content,
				'category_id':category,
				'sub_category_id':sub_category,
				'usub_category_id':usub_category,
				'is_downloadable':is_downloadable,
				'posted_date':posted_date,
			};
			// alert(JSON.stringify(data));
			if($('#add_poster #poster_file').val() == ''){
				app.alert('Required','Please Upload File');
			}else if(app.getFileExtension($('#add_poster #poster_file').val()) == 'pdf' && $('#add_poster #poster_file_thumb').val() == ''){
					app.alert('Required','If you were uploading pdf please insert thumbnail');
			}
			else{
				$.ajax({
	                type:'POST',
	                dataType:'JSON',
	                url:base_url+"cms/submit_add_poster",
	                data:data,
	                success:function(data)
	                {
	                	if(data == 1){
		                    app.alert_redirection('Success','Poster Added',base_url+'cms/posters');

	                	}else{
	                		app.alert('Error','There`s something wrong please reload the page');
	                	}
	                }
	          	});
			}

		});


		$(document).on('submit','#edit_poster',function(e){
			e.preventDefault();
			var poster_file = $('#edit_poster #poster_file').val();
			var poster_file_thumb = $('#edit_poster #poster_file_thumb').val();
			var poster_by = $('#edit_poster #poster_by').val();
			var poster_title = $('#edit_poster #poster_title').val();
			var poster_content = $('#edit_poster #poster_content').val();
			var category = $('#edit_poster #category').val();
			var sub_category = $('#edit_poster #sub_category').val();
			var usub_category = $('#edit_poster #usub_category').val();
			var is_downloadable = $('#edit_poster #is_downloadable').val();
			var posted_date = $('#edit_poster #posted_date').val();
			var id = $('#edit_poster #id').val();
			var data = {
				'poster_file_thumb':poster_file_thumb,
				'poster_file':poster_file,
				'poster_by':poster_by,
				'poster_name':poster_title,
				'poster_content':poster_content,
				'category_id':category,
				'sub_category_id':sub_category,
				'usub_category_id':usub_category,
				'is_downloadable':is_downloadable,
				'posted_date':posted_date,
				'id':id,
			};
			// alert(JSON.stringify(data));
			if($('#edit_poster #poster_file').val() == ''){
				app.alert('Required','Please Upload File');
			}else{
				$.ajax({
	                type:'POST',
	                dataType:'JSON',
	                url:base_url+"cms/submit_edit_poster",
	                data:data,
	                success:function(data)
	                {
	                	if(data == 1){
		                    app.alert_redirection('Success','Poster Updated',base_url+'cms/posters');

	                	}else{
	                		app.alert('Error','There`s something wrong please reload the page');
	                	}
	                }
	          	});
			}

		});

		$(document).on('submit','#delete_poster',function(e){
			e.preventDefault();
			var id = $('#delete_poster #id').val();
			var data = {'id':id};
			// alert(JSON.stringify(data));
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"cms/delete_poster",
                data:data,
                success:function(data)
                {
                	if(data == 1){
	                    app.alert_redirection('Success','Poster Deleted',base_url+"cms/posters");

                	}else{
                		app.alert('Error','There`s something wrong please reload the page');
                	}
                }
          	});
		});

		$('#upload_file').click(function(){
			$('#file').click();
		});

		$('#upload_file2').click(function(){
			$('#file2').click();
		});

		$('#get_my_name').click(function(){
			$.ajax(base_url+'Admin/get_admin_session_array', {
		        method: "POST",
		        data: {},
		        dataType: "json",
		        success: function (data) {
		    		// alert(JSON.stringify(data));
		    		$('#poster_by').val(data.fname+' '+data.mname+' '+data.lname);
		    		$('#edit_poster #poster_by').val(data.fname+' '+data.mname+' '+data.lname);
		        }
		    });
		});

		$('#close_file').click(function(){
			$.ajax(base_url+'cms/remove_cropped_thumbnail', {
		        method: "GET",
		        data: {'thumb_image':$('#poster_file').val()},
		        dataType: "json",
		        success: function (result) {
		    		if(app.getFileExtension($('#poster_file').val()) != 'pdf'){
		        			$('.cover_upload2').hide();
		        			$('.cover_upload2 img').attr('src', '');
		        	}else{
		        			$('.cover_upload3').hide();
	                		$('.cover_upload3 embed').attr('src', '');
	                		$('.cover_upload3 embed').append(data.file_data.file_name);
	                		$('.cover_upload3 embed').attr('target', '_blank');
		        	}
		        	$('.cover_upload').show();
		        	$('#poster_file').val('');

		        }
		    });

		});

		$('#close_file2').click(function(){
			$.ajax(base_url+'cms/remove_cropped_thumbnail', {
		        method: "GET",
		        data: {'thumb_image':$('#poster_file').val()},
		        dataType: "json",
		        success: function (result) {
		    		if(app.getFileExtension($('#poster_file').val()) != 'pdf'){
		        			$('.cover_upload2').hide();
		        			$('.cover_upload2 img').attr('src', '');
		        	}else{
		        			$('.cover_upload4').hide();
		        			$('.cover_upload3').hide();
	                		$('.cover_upload3 embed').attr('src', '');
	                		$('.cover_upload3 embed').html('');
	                		$('.cover_upload3 embed').attr('target', '_blank');
		        	}
		        	$('.cover_upload').show();
		        	$('#poster_file').val('');

		        }
		    });

		});

		$('#close_file3').click(function(){
			$.ajax(base_url+'cms/remove_cropped_thumbnail', {
		        method: "GET",
		        data: {'thumb_image':$('#poster_file_thumb').val()},
		        dataType: "json",
		        success: function (result) {
    				$('.cover_upload5').hide();
        			$('.cover_upload5 img').attr('src', '');
		        	
		        	$('.cover_upload4').show();
		        	$('#poster_file_thumb').val('');

		        }
		    });

		});

		$(document).on('change','#file', function(){

        	app.upload();
		});

		$(document).on('change','#file2', function(){

        	app.upload2();
		});

		$(document).on('change','#add_poster #category', function(){
			var data = {'id':$(this).val()}
			var id = 1;
        	$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"Admin/get_array_sub_category",
                data:data,
                success:function(data)
                {

                	// alert(JSON.stringify(data));
                	$('#add_poster #sub_category').html('');
                	$.each(data,function(key, value)
	                {
	                	if(id == 1){
	                		$.ajax({
				                type:'POST',
				                dataType:'JSON',
				                url:base_url+"Admin/get_array_usub_category",
				                data:{'id':value.sc_id},
				                success:function(data2)
				                {
				                	// alert(JSON.stringify(data2));
				                	$('#add_poster #usub_category').html('');
				                	$.each(data2,function(key, value)
					                {
					                    $('#add_poster #usub_category').append('<option value=' + value.usc_id + '>' + value.usname + '</option>'); // return empty
					                });
				                }
				                // ,error: function(XMLHttpRequest, textStatus, errorThrown){
				                // 	alert();
				                // }
				          	});
	                	}
	                	id = id + 1;
	                    $('#add_poster #sub_category').append('<option value=' + value.sc_id + '>' + value.sname + '</option>'); // return empty
	                });
                }
          	});
		});

		$(document).on('change','#edit_poster #category', function(){
			var data = {'id':$(this).val()}
			var id = 1;
        	$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"Admin/get_array_sub_category",
                data:data,
                success:function(data)
                {

                	// alert(JSON.stringify(data));
                	$('#edit_poster #sub_category').html('');
                	$.each(data,function(key, value)
	                {
	                	if(id == 1){
	                		$.ajax({
				                type:'POST',
				                dataType:'JSON',
				                url:base_url+"Admin/get_array_usub_category",
				                data:{'id':value.sc_id},
				                success:function(data2)
				                {
				                	// alert(JSON.stringify(data2));
				                	$('#edit_poster #usub_category').html('');
				                	$.each(data2,function(key, value)
					                {
					                    $('#edit_poster #usub_category').append('<option value=' + value.usc_id + '>' + value.usname + '</option>'); // return empty
					                });
				                }
				                // ,error: function(XMLHttpRequest, textStatus, errorThrown){
				                // 	alert();
				                // }
				          	});
	                	}
	                	id = id + 1;
	                    $('#edit_poster #sub_category').append('<option value=' + value.sc_id + '>' + value.sname + '</option>'); // return empty
	                });
                }
          	});
		});

		$(document).on('change','#add_poster #sub_category', function(){
			var data = {'id':$(this).val()}
        	$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"Admin/get_array_usub_category",
                data:data,
                success:function(data)
                {
                	// alert(JSON.stringify(data));
                	$('#add_poster #usub_category').html('');
                	$.each(data,function(key, value)
	                {
	                    $('#add_poster #usub_category').append('<option value=' + value.usc_id + '>' + value.usname + '</option>'); // return empty
	                });
                }
          	});
		});

		$(document).on('change','#edit_poster #sub_category', function(){
			var data = {'id':$(this).val()}
        	$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+"Admin/get_array_usub_category",
                data:data,
                success:function(data)
                {
                	// alert(JSON.stringify(data));
                	$('#edit_poster #usub_category').html('');
                	$.each(data,function(key, value)
	                {
	                    $('#edit_poster #usub_category').append('<option value=' + value.usc_id + '>' + value.usname + '</option>'); // return empty
	                });
                }
          	});
		});


	},
	upload: function() {
	    var uploadForm    = $('#uploadForm'),
	        // progressEl    = $('#progressBar.progress'),
	        // progressbar   = $('#progressBar .progress-bar'),
	        file          = $('input#file'),
	        preview       = $('#preview');

        uploadForm.ajaxSubmit({
        	dataType: "json",
            beforeSubmit: function(data) {
                // if(file.val() !== ''){
                //     progressEl.show();
                // }

                // //Display filename
                // var x = '<div class="uploadedFilename">';
                //         x += '<div>'+data[0].value.name+'</div>';
                //     x += '</div>';

                // preview.html(x);
            },
            uploadProgress: function (event, position, total, progress){
                // var x = '<div class="progress-bar progress-bar active" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: '+progress+'%"></div>';
                // progressEl.html(x);
            },
            success:function (data){

            	// progressEl.hide();
              if(data.status !== 'success'){

                    if (data.msg == "<p>The filetype you are attempting to upload is not allowed.</p>") {
                        var pop_msg = "<p>"+'Invalid file type upload files in PDF,png, jpeg or jpg format only'+"</p>";
                    }else{
                        var pop_msg = data.msg;
                    }

                    $.alert({
                        scrollToPreviousElement:false,
                        backgroundDismiss: false,
                        closeIcon: true,
                        closeIconClass: 'fa fa-close',
                        titleClass: 'dialogHeader',
                        draggable: false,
                        animation: 'none',
                        backgroundDismissAnimation: 'none',
                        columnClass: 'small',
                        title: '<div>'+'failed'+'</div>',
                        content: '<div class="dialogContent">'+pop_msg+'</div>',
                        buttons: {
                            Ok: {
								text: 'ok',
                                btnClass: 'clearButton',
                            }
                        },
                    });


                } else {
                	// data.file_data
                	$('.cover_upload').hide();
                	$('#poster_file').val(data.file_data.file_name);
					if(app.getFileExtension(data.file_data.file_name) != 'pdf'){
	        			$('.cover_upload2').show();
	                	$('.cover_upload2 img').attr('src', base_url+'uploads/'+data.file_data.file_name);
                	}else{
                		$('.cover_upload3 embed').attr('src', base_url+'uploads/'+data.file_data.file_name);
                		$('.cover_upload3 embed').append(data.file_data.file_name);
                		$('.cover_upload3 embed').attr('target', '_blank');
                		$('.cover_upload4').show();
                		$('.cover_upload3').show();
                		
                	}
                    // ./Hide & Show Element
                }
            },
            resetForm: true
        });
    },


    upload2: function() {
	    var uploadForm    = $('#uploadForm2'),
	        file          = $('input#file2'),
	        preview       = $('#preview');

        uploadForm.ajaxSubmit({
        	dataType: "json",
            beforeSubmit: function(data) {
                
            },
            uploadProgress: function (event, position, total, progress){
                
            },
            success:function (data){

              if(data.status !== 'success'){

                    if (data.msg == "<p>The filetype you are attempting to upload is not allowed.</p>") {
                        var pop_msg = "<p>"+'Invalid file type upload files in png, jpeg or jpg format only'+"</p>";
                    }else{
                        var pop_msg = data.msg;
                    }

                    $.alert({
                        scrollToPreviousElement:false,
                        backgroundDismiss: false,
                        closeIcon: true,
                        closeIconClass: 'fa fa-close',
                        titleClass: 'dialogHeader',
                        draggable: false,
                        animation: 'none',
                        backgroundDismissAnimation: 'none',
                        columnClass: 'small',
                        title: '<div>'+'failed'+'</div>',
                        content: '<div class="dialogContent">'+pop_msg+'</div>',
                        buttons: {
                            Ok: {
								text: 'ok',
                                btnClass: 'clearButton',
                            }
                        },
                    });


                } else {
                	// data.file_data
                	$('.cover_upload4').hide();
                	$('#poster_file_thumb').val(data.file_data.file_name);
						$('.cover_upload5').show();
	                	$('.cover_upload5 img').attr('src', base_url+'uploads/'+data.file_data.file_name);
                    // ./Hide & Show Element
                }
            },
            resetForm: true
        });
    },

    getFileExtension: function(filename){
	  var ext = /^.+\.([^.]+)$/.exec(filename);
	  return ext == null ? "" : ext[1];
	},
};
