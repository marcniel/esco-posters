<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_class');
	}
	public function index(){
		$data = array(
			'title' => 'Esco Poster - dashboard',
			'page'	=> 'dashboard',
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/index", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}

	public function users(){
		$data = array(
			'title' => 'Esco Poster - users',
			'page'	=> 'users',
			'users' => $this->Admin_class->get_users()
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/users", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}

	public function downloads(){
		$data = array(
			'title' => 'Esco Poster - download history',
			'page'	=> 'downloads',
			'downloads' => $this->Admin_class->get_downloads()
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/downloads", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}


	public function reports(){
		$data = array(
			'title' => 'Esco Poster - reports',
			'page'	=> 'reports',
			'reports' => $this->Admin_class->get_reports()
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/reports", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}

	public function add_user(){
		$post = $this->input->post();
		$data = array(
			'fname' 			=> $post['fname'],
			'mname' 			=> $post['mname'],
			'lname' 			=> $post['lname'],
			'number' 			=> $post['number'],
			'email' 			=> $post['email'],
			'password' 			=> $post['password'],
			'verification_key' 	=> $post['verification_key'],
			'is_verified' 		=> $post['is_verified'],
		);
		$val = $this->Admin_class->add_user($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function send_email(){
		$post = $this->input->post();
		$data = array(
			'subject' 			=> $post['subject'],
			'email_content' 	=> $post['email_content'],
			'email' 			=> $post['email'],
		);
		$val = $this->Admin_class->send_email($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function edit_user(){
		$post = $this->input->post();
		$data = array(
			'fname' 			=> $post['fname'],
			'mname' 			=> $post['mname'],
			'lname' 			=> $post['lname'],
			'number' 			=> $post['number'],
			'email' 			=> $post['email'],
			'old_email' 			=> $post['old_email'],
			'password' 			=> $post['password'],
			'id' 				=> $post['id'],
		);
		$val = $this->Admin_class->edit_user($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function delete_user(){
		$post = $this->input->post();
		$data = array(
			'id' 	=> $post['id'],
		);
		$val = $this->Admin_class->delete_user($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode($val);
		}
	}

	public function posters(){
		$data = array(
			'title' => 'Esco Poster - posters',
			'page'	=> 'posters',
			'posters' => $this->Admin_class->get_posters(),
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/posters", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}


	public function add_poster(){
		$data = array(
			'title' => 'Esco Poster - add posters',
			'page'	=> 'posters',
			'posters' => $this->Admin_class->get_posters(),
			'category' => $this->Admin_class->get_category()
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/add_poster", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}

	public function edit_poster($id=null){
		$data = array(
			'title' => 'Esco Poster - edit posters',
			'page'	=> 'posters',
			'posters' => $this->Admin_class->get_posters(),
			'get_poster' => $this->Admin_class->get_posters_edit($id),
			'category' => $this->Admin_class->get_category()
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/edit_poster", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}

	public function submit_add_poster(){
		$post = $this->input->post();
		$data = array(
			'poster_file'		=> $post['poster_file'],
			'poster_file_thumb'	=> $post['poster_file_thumb'],
			'poster_by'			=> $post['poster_by'],
			'poster_name'		=> $post['poster_name'],
			'poster_content'	=> $post['poster_content'],
			'category_id'		=> $post['category_id'],
			'sub_category_id'	=> $post['sub_category_id'],
			'usub_category_id'	=> $post['usub_category_id'],
			'is_downloadable'	=> $post['is_downloadable'],
			'posted_date'		=> $post['posted_date'],
		);
		$val = $this->Admin_class->submit_add_poster($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function submit_edit_poster(){
		$post = $this->input->post();
		$data = array(
			'poster_file'		=> $post['poster_file'],
			'poster_file_thumb'	=> $post['poster_file_thumb'],
			'poster_by'			=> $post['poster_by'],
			'poster_name'		=> $post['poster_name'],
			'poster_content'	=> $post['poster_content'],
			'category_id'		=> $post['category_id'],
			'sub_category_id'	=> $post['sub_category_id'],
			'usub_category_id'	=> $post['usub_category_id'],
			'is_downloadable'	=> $post['is_downloadable'],
			'posted_date'		=> $post['posted_date'],
			'id'				=> $post['id'],
		);
		$val = $this->Admin_class->submit_edit_poster($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function delete_poster(){
		$post = $this->input->post();
		$data = array(
			'id' 	=> $post['id'],
		);
		$val = $this->Admin_class->delete_poster($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode($val);
		}
	}



	public function profile(){
		$data = array(
			'title' => 'Esco Poster - profile',
			'page'	=> 'profile',
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/profile", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}

	public function admin_profile(){
		$post = $this->input->post();
		$data = array(
			'lname' 	=> $post['lname'],
			'fname'		=> $post['fname'],
			'mname' 	=> $post['mname'],
			'email'		=> $post['email'],
			'password' 	=> $post['password'],
		);
		$val = $this->Admin_class->admin_profile($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode($val);
		}
	}

	public function upload_image()
	{
		$post = $this->input->post();
		$data = array(
			'image' 	=> $post['upload_image'],
		);
		$val = upload_image($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode($val);
		}

	}
	public function categories(){
		$data = array(
			'title' => 'Esco Poster - categories',
			'page'	=> 'categories',
			'category_type' => 'category',
			'category' => $this->Admin_class->get_category()
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/categories", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}

	public function add_category(){
		$post = $this->input->post();
		$data = array(
			'category' 	=> $post['category'],
		);
		$val = $this->Admin_class->add_category($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}


	public function edit_category(){
		$post = $this->input->post();
		$data = array(
			'category' 	=> $post['category'],
			'id' 		=> $post['id'],
		);
		$val = $this->Admin_class->edit_category($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function delcategory(){
		$post = $this->input->post();
		$data = array(
			'id' 	=> $post['id'],
		);
		$val = $this->Admin_class->delcategory($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode($val);
		}
	}


	//sub_category sub_categories

	public function sub_categories(){
		$data = array(
			'title' => 'Esco Poster - sub category',
			'page'	=> 'categories',
			'category_type' => 'subcategory',
			'sub_category' => $this->Admin_class->get_sub_category(),
			'category' => $this->Admin_class->get_category()
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/sub_categories", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}

	public function add_sub_category(){
		$post = $this->input->post();
		$data = array(
			'category' 	=> $post['category'],
			'sub_category' => $post['sub_category'],
		);
		$val = $this->Admin_class->add_sub_category($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function add_usub_category(){
		$post = $this->input->post();
		$data = array(
			'sub_category' 	=> $post['sub_category'],
			'usub_category' => $post['usub_category'],
		);
		$val = $this->Admin_class->add_usub_category($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}


	public function admin_forget_password(){
		$post = $this->input->post();
		$data = array(
			'email' 	=> $post['email'],
		);
		$val = $this->Admin_class->admin_forget_password($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function editsubcategory(){
		$post = $this->input->post();
		$data = array(
			'category' 	=> $post['category'],
			'sub_category' => $post['sub_category'],
			'id' 		=> $post['id'],
		);
		$val = $this->Admin_class->editsubcategory($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function edit_usub_category(){
		$post = $this->input->post();
		$data = array(
			'sub_category' 	=> $post['sub_category'],
			'usub_category' => $post['usub_category'],
			'id' 			=> $post['id'],
		);
		$val = $this->Admin_class->edit_usub_category($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function delete_sub_category(){
		$post = $this->input->post();
		$data = array(
			'id' 	=> $post['id'],
		);
		$val = $this->Admin_class->delete_sub_category($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function delete_usub_category(){
		$post = $this->input->post();
		$data = array(
			'id' 	=> $post['id'],
		);
		$val = $this->Admin_class->delete_usub_category($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	//sub_category under sub_categories

	public function usub_categories(){
		$data = array(
			'title' => 'Esco Poster - under sub category',
			'page'	=> 'categories',
			'category_type' => 'usubcategory',
			'usub_category' => $this->Admin_class->get_usub_category(),
			'sub_category' => $this->Admin_class->get_sub_category(),
			'category' => $this->Admin_class->get_category()
		);
		$this->load->view("admin/global/header", $data);
		$this->load->view("admin/global/nav", $data);
		$this->load->view("admin/usub_categories", $data);
		$this->load->view("admin/global/footer", $data);
		admin_session_redirection();
	}


	public function login(){
		$data = array(
			'title' => 'Esco Poster - login',
			'page'	=> 'login',
		);
		$this->load->view("admin/global/header", $data);
		// $this->load->view("admin/global/nav", $data);
		$this->load->view("admin/login", $data);
		$this->load->view("admin/global/footer", $data);
	}

	public function login_submit(){
		$post = $this->input->post();
		$data = array(
			'email' 	=> $post['email'],
			'password'	=> $post['password']
		);
		$val = $this->Admin_class->users_login($data);
		if($val != 0){
			$this->session->set_userdata('admin',$val);
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function get_data(){
		$post = $this->input->post();
		$val = get_my_table($post['table'],$post['id'],$post['field']);
			echo json_encode($val);
	}

	public function get_user_array(){
		$post = $this->input->post();
		$val = get_user_array($post['id']);
			echo json_encode($val);
	}

	public function get_admin_session_array(){
		$val = get_admin_session_array();
			echo json_encode($val);
	}

	public function get_array_sub_category(){
		$post = $this->input->post();
		$val = get_array_sub_category($post['id']);
			echo json_encode($val);
	}

	public function get_array_usub_category(){
		$post = $this->input->post();
		$val = get_array_usub_category($post['id']);
			echo json_encode($val);
	}



	public function get_user_password(){
		$post = $this->input->post();
		$val = get_user_password($post['id']);
			echo json_encode($val);
	}



	public function logout()
	{
		$this->session->unset_userdata('admin');
		redirect(base_url().'cms/login', 'location');

	}


	public function upload_resume()
    {
        $user_dir_resume    = './uploads';
        if(!file_exists($user_dir_resume)){
            mkdir( $user_dir_resume, 0755 );
        }

        $file_path = 'uploads/';

        //Upload Config
        $config['upload_path'] = $file_path;
        $config['allowed_types'] = 'pdf|png|jpeg|jpg';//'png|jpeg|jpg';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = (1024*15); //15 MB
        // ./Upload Config

        $this->upload->initialize($config);

        if (isset($_FILES['file']['name'])) {

            // Get Image Size
            //list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
            // ./ Get Image Size

            if (0 < $_FILES['file']['error']) {
                $result = array(
                    'status'    => 'error',
                    'msg'       => 'Error occured during file upload.',
                    'file_data' => ''
                );
            } else {
                if (file_exists($file_path. $_FILES['file']['name'])) {
                    $result = array(
                        'status'    => 'existing',
                        'msg'       => 'File already exists.',
                        'file_data' => ''
                    );
                } else {

                    if (!$this->upload->do_upload('file')) {
                        $result = array(
                            'status'    => 'error',
                            'msg'       => $this->upload->display_errors(),
                            'file_data' => ''
                        );
                    } else {

                        //Unlink Thumb
                       // $this->Files_class->unlink_banner($thumbfile);

                        $upload_data = $this->upload->data();
                        $result = array(
                            'status'    => 'success',
                            'msg'       => 'File successfully uploaded.',
                            'file_data' => $upload_data,
                        );
                        $this->session->set_userdata('last_uploaded_banner', $upload_data['file_name']);
                    }
                }
            }
        } else {
            $result = array(
                'status'    => 'blank',
                'msg'       => 'Please choose a file.',
                'file_data' => ''
            );
        }
        echo json_encode($result);
    }


    public function upload_resume2()
    {
        $user_dir_resume    = './uploads';
        if(!file_exists($user_dir_resume)){
            mkdir( $user_dir_resume, 0755 );
        }

        $file_path = 'uploads/';

        //Upload Config
        $config['upload_path'] = $file_path;
        $config['allowed_types'] = 'png|jpeg|jpg';//'png|jpeg|jpg';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = (1024*15); //15 MB
        $this->upload->initialize($config);
        if (isset($_FILES['file2']['name'])) {
            if (0 < $_FILES['file2']['error']) {
                $result = array(
                    'status'    => 'error',
                    'msg'       => 'Error occured during file upload.',
                    'file_data' => ''
                );
            } else {
                if (file_exists($file_path. $_FILES['file2']['name'])) {
                    $result = array(
                        'status'    => 'existing',
                        'msg'       => 'File already exists.',
                        'file_data' => ''
                    );
                } else {
                    if (!$this->upload->do_upload('file2')) {
                        $result = array(
                            'status'    => 'error',
                            'msg'       => $this->upload->display_errors(),
                            'file_data' => ''
                        );
                    } else {
                    	$upload_data = $this->upload->data();
                        $result = array(
                            'status'    => 'success',
                            'msg'       => 'File successfully uploaded.',
                            'file_data' => $upload_data,
                        );
                        $this->session->set_userdata('last_uploaded_banner', $upload_data['file_name']);
                    }
                }
            }
        } else {
            $result = array(
                'status'    => 'blank',
                'msg'       => 'Please choose a file.',
                'file_data' => ''
            );
        }
        echo json_encode($result);
    }

    public function remove_cropped_thumbnail()
    {
        $filename = $this->input->get('thumb_image');


            $unlink = @unlink('./uploads/'.$filename);

        if($unlink){
            $result = array(
                'status'    => 'success',
            );
        } else {
            $result = array(
                'status'    => 'error',
            );
        }
        echo json_encode($result);
    }

		public function get_poster_counts()
		{
			if ($this->input->is_ajax_request()) {
				$post = $this->input->post();

				$result = $this->Admin_class->get_poster_counts(12);

				echo json_encode($result);
			}
		}

}
