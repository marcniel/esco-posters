<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	public function __construct(){
		parent::__construct();
		require_once APPPATH.'third_party/src/Google_Client.php';
		require_once APPPATH.'third_party/src/contrib/Google_Oauth2Service.php';
		$this->load->model('Users_class');
	}

	public function index(){
		$data = array(
			'title' => 'Esco Poster',
			'page'	=> 'home',
			'login_button' => '<a class="btn btn-info google_icon" href="'.base_url().'Users/loggedin"><img src="'.base_url().'assets/images/google.png" /> <span>Sign in with Google</span></a>',
		);
		$this->load->view("global/header", $data);
		$this->load->view("home", $data);
		$this->load->view("global/footer", $data);

	}

	public function uploading($id=null){


		$data = array(
			'title' => 'Esco Poster - poster view',
			'page'	=> 'poster_view',
			'get_poster' => $this->Users_class->get_posters_id($id),
			'login_button' => '<a class="btn btn-info google_icon" href="'.base_url().'Users/loggedin"><img src="'.base_url().'assets/images/google.png" /> <span>Sign in with Google</span></a>',
		);

		$view_data = array(
		    'view_by' 			=> user_session_val(),
			'view_poster' 		=> $id,
		);
		add_views($view_data);

		$this->load->view("global/header", $data);
		$this->load->view("poster_view", $data);
		$this->load->view("global/footer", $data);
		
	}

	public function profile(){
		$data = array(
			'title' => 'Esco Poster - profile',
			'page'	=> 'profile',
		);
		$this->load->view("global/header", $data);
		$this->load->view("profile", $data);
		$this->load->view("global/footer", $data);
	}

	public function get_posters_name(){
		$get_poster = $this->Users_class->get_posters_name();
		echo json_encode($get_poster);
	}

	public function posters(){
		  $get = $this->input->get();
		  $data['total_page'] = 0;

		  //pagination

		  $total_rows = count(get_array_poster(
				(isset($get['cid']))? $get['cid']: '',
				(isset($get['sc_id']))? $get['sc_id']: '',
				(isset($get['usc_id']))? $get['usc_id']: '',
				(isset($get['type']))? $get['type']: '',
				(isset($get['q']))? $get['q']: '',
				(isset($get['d']))? $get['d']: '',
				(isset($get['sort']))? $get['sort']: '','',''));

					$base_url = base_url().'posters?paginate=true'.
					((isset($get['cid']))? '&cid='.$get['cid']: '').
					((isset($get['sc_id']))? '&sc_id='.$get['sc_id']: '').
					((isset($get['usc_id']))? '&usc_id='.$get['usc_id']: '').
					((isset($get['type']))? '&type='.$get['type']: '').
					((isset($get['q']))? '&q='.$get['q']: '').
					((isset($get['d']))? '&d='.$get['d']: '').
					((isset($get['sort']))? '&sort='.$get['sort']: '');

					$config = array(
						'page_query_string' => TRUE,
						'base_url' => $base_url,
						'total_rows' => $total_rows,
						'per_page' => 6,
						'num_links' => 2,
						'full_tag_open' => '<ul class="pagination">',
						'full_tag_close' => '</ul>',
						'prev_link' => '<i class="fa fa-angle-left"></i> '.'Prev',
						'prev_tag_open' => '<li>',
						'prev_tag_close' => '</li>',
						'next_tag_open' => '<li>',
						'next_tag_close' => '</li>',
						'cur_tag_open' => '<li class="active"><a href="javascript:;">',
						'cur_tag_close' => '</a></li>',
						'num_tag_open' => '<li>',
						'num_tag_close' => '</li>',
						'next_link' => 'Next'.' <i class="fa fa-angle-right"></i>',
						'first_link' => '<button class="firstbuttonpage"> <i class="fa fa-angle-double-left"></i> First</button>',
						'last_link' => '<button class="lastbuttonpage">Last <i class="fa fa-angle-double-right"></i></button>'
					);

					$this->pagination->initialize($config);
                    $offset = !empty($get['per_page']) ? $get['per_page'] : 0;
                    $data['total_page'] = 0;

		  //end of pagination


		$data = array(
			'title' => 'Esco Poster - posters',
			'page'	=> 'posters',
			'posters_list' => get_array_poster(
				(isset($get['cid']))? $get['cid']: '',
				(isset($get['sc_id']))? $get['sc_id']: '',
				(isset($get['usc_id']))? $get['usc_id']: '',
				(isset($get['type']))? $get['type']: '',
				(isset($get['q']))? $get['q']: '',
				(isset($get['d']))? $get['d']: '',
				(isset($get['sort']))? $get['sort']: '',$config['per_page'], $offset),
			'login_button' => '<a class="btn btn-info google_icon" href="'.base_url().'Users/loggedin"><img src="'.base_url().'assets/images/google.png" /> <span>Sign in with Google</span></a>',
			'page_links'    => explode('&nbsp;', $this->pagination->create_links()),
            'total_page'    => ceil($config['total_rows'] / $config['per_page']),
            'curr_num_page' => (ceil($offset/$config['per_page'])-$data['total_page'])+1,

		);
		$this->load->view("global/header", $data);
		$this->load->view("posters", $data);
		$this->load->view("global/footer", $data);
	}

	public function upload_image_user()
	{
		$post = $this->input->post();
		$data = array(
			'image' 	=> $post['upload_image_user'],
		);
		$val = upload_image_user($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode($val);
		}
		
	}

	public function verify(){
		$data = array(
			'title' => 'Esco Poster',
			'page'	=> 'verification',
			'send_sms_data' => $this->Users_class->send_sms(),
		);
		$this->load->view("global/header", $data);
		$this->load->view("verification", $data);
		$this->load->view("global/footer", $data);

	}

	public function sign_up(){
		$post = $this->input->post();
		$data = array(
			'fname' 			=> $post['fname'],
			'mname' 			=> $post['mname'],
			'lname' 			=> $post['lname'],
			'number' 			=> $post['number'],
			'email' 			=> $post['email'],
			'password' 			=> $post['password'],
			'verification_key' 	=> $post['verification_key'],
			'is_verified' 		=> $post['is_verified'],
		);
		$val = $this->Users_class->sign_up($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}


	public function send_report(){
		$post = $this->input->post();
		$data = array(
			'fname' 			=> $post['fname'],
			'lname' 			=> $post['lname'],
			'subject' 			=> $post['subject'],
			'email' 			=> $post['email'],
			'content' 			=> $post['content'],
		);
		$val = $this->Users_class->send_report($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}


	public function login_user(){
		$post = $this->input->post();
		$data = array(
			'email' 			=> $post['email'],
			'password' 			=> $post['password'],
		);
		$val = $this->Users_class->login_user($data);
		if($val != 0){
			$this->session->set_userdata('user',$val);
			if(user_verified($val)){
				echo json_encode(1);
			}else{
				echo json_encode(2);
			}
		}else{
			echo json_encode(0);
		}
	}


	public function poster_download(){
		$post = $this->input->post();
		$data = array(
			'poster_id' 		=> $post['poster_id'],
			'get_by' 			=> $post['get_by'],
		);
		$val = $this->Users_class->poster_download($data);
		if($val != 0){
			echo json_encode(1);
			
		}else{
			echo json_encode(0);
		}
	}

	public function verify_user(){
		$val = $this->Users_class->verify_user();
		if($val != 0){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function logout()
	{
		//google auth
		$this->session->unset_userdata('access_token');
  		$this->session->unset_userdata('user_data');

		$this->session->unset_userdata('user');
		redirect(base_url(), 'location');
		
	}

	public function users_forget_password(){
		$post = $this->input->post();
		$data = array(
			'email' 	=> $post['email'],
		);
		$val = $this->Users_class->users_forget_password($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	public function user_profile(){
		$post = $this->input->post();
		$data = array(
			'lname' 	=> $post['lname'],
			'fname'		=> $post['fname'],
			'mname' 	=> $post['mname'],
			'email'		=> $post['email'],
			'password' 	=> $post['password'],
			'number' 	=> $post['number'],
		);
		$val = $this->Users_class->user_profile($data);
		if($val == 1){
			echo json_encode(1);
		}else{
			echo json_encode($val);
		}
	}

	public function get_report_array(){
		$post = $this->input->post();
		$val = get_report_array($post['id']);
			echo json_encode($val);
	}

	public function loggedin(){
		$clientId = '938498046100-nqj2iv3be9tt0i0ldt30vnap8tqpqa1e.apps.googleusercontent.com'; //Google client ID
		$clientSecret = 'NveOqt8UtKDCh5Sh6BdNdWNm'; //Google client secret
		$redirectURL = base_url().'Users/loggedin';
		
		//https://curl.haxx.se/docs/caextract.html

		//Call Google API
		$gClient = new Google_Client();
		$gClient->setApplicationName('Login');
		$gClient->setClientId($clientId);
		$gClient->setClientSecret($clientSecret);
		$gClient->setRedirectUri($redirectURL);
		$google_oauthV2 = new Google_Oauth2Service($gClient);
		
		if(isset($_GET['code']))
		{
			$gClient->authenticate($_GET['code']);
			$_SESSION['token'] = $gClient->getAccessToken();
			header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
		}

		if (isset($_SESSION['token'])) 
		{
			$gClient->setAccessToken($_SESSION['token']);
		}
		
		if ($gClient->getAccessToken()) {
            $data = $google_oauthV2->userinfo->get();
            $current_datetime = date('Y-m-d');
			if($this->google_login_model->Is_already_register($data['id']))
		    {
		     $user_data = array(
		      'fname' => $data['given_name'],
		      'lname'  => $data['family_name'],
		      'email' => $data['email'],
		      'date_updated' => $current_datetime
		     );
		     $this->google_login_model->Update_user_data($user_data, $data['id']);
		    }
		    else
		    {
		     $user_data = array(
		      'login_oauth_uid' => $data['id'],
		      'fname'  => $data['given_name'],
		      'lname'   => $data['family_name'],
		      'email'  => $data['email'],
		      'image' => $data['picture'],
		      'verification_key' => rand(),
		      'is_verified' => '0',
		      'date_added'  => $current_datetime
		     );
		     $this->google_login_model->Insert_user_data($user_data);
		    }
		    $this->session->set_userdata('user', get_gmail_by_oauth($data['id'],'id'));
		    $this->session->set_userdata('user_data', $user_data);
		    redirect(base_url(), 'location');
        } 
		else 
		{
            $url = $gClient->createAuthUrl();
		    header("Location: $url");
            exit;
        }
	}
}
?>
