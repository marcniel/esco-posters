<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route = array(
	'default_controller' => 'Users',


	//sign_up
	'users/sign_up' 				=> 'Users/sign_up',
	'users/send_report' 			=> 'Users/send_report',
	'users/login_user' 				=> 'Users/login_user',
	'users/users_forget_password' 	=> 'Users/users_forget_password',
	'uploading/(:any)' 				=> 'Users/uploading/$1',
	'verification' 					=> 'Users/verify',
	'users/verify_user' 			=> 'Users/verify_user',
	'users/upload_image_user' 		=> 'Users/upload_image_user',
	'users/poster_download' 		=> 'Users/poster_download',
	'profile' 						=> 'Users/profile',
	'posters' 						=> 'Users/posters',
	'users/user_profile'			=> 'Users/user_profile',

	'login_google'					=> 'Users/login_google',

	// cms
	'cms' 					=> 'Admin/index',
	'cms/get_poster_counts' => 'Admin/get_poster_counts',
	'cms/login' 			=> 'Admin/login',
	'cms/login_submit'		=> 'Admin/login_submit',
	'cms/profile'			=> 'Admin/profile',
	'cms/admin_profile'		=> 'Admin/admin_profile',
	'cms/upload_image'		=> 'Admin/upload_image',
	'cms/logout'			=> 'Admin/logout',
	'cms/downloads'			=> 'Admin/downloads',
	'cms/reports'			=> 'Admin/reports',
	'cms/send_email'		=> 'Admin/send_email',

	//cms categories
	'cms/categories'		=> 'Admin/categories',
	'cms/add_category'		=> 'Admin/add_category',
	'cms/delcategory'		=> 'Admin/delcategory',
	'cms/edit_category'		=> 'Admin/edit_category',

	//cms sub categories
	'cms/sub_category'			=> 'Admin/sub_categories',
	'cms/add_sub_category'		=> 'Admin/add_sub_category',
	'cms/editsubcategory'		=> 'Admin/editsubcategory',
	'cms/delete_sub_category'	=> 'Admin/delete_sub_category',

	//cms under sub categories
	'cms/usub_category'			=> 'Admin/usub_categories',
	'cms/add_usub_category'		=> 'Admin/add_usub_category',
	'cms/edit_usub_category'	=> 'Admin/edit_usub_category',
	'cms/delete_usub_category'	=> 'Admin/delete_usub_category',


	//cms users
	'cms/users'		 			=> 'Admin/users',
	'cms/admin_forget_password'	=> 'Admin/admin_forget_password',
	'cms/add_user'		 		=> 'Admin/add_user',
	'cms/edit_user'		 		=> 'Admin/edit_user',
	'cms/delete_user'		 	=> 'Admin/delete_user',
	'cms/remove_cropped_thumbnail' => 'Admin/remove_cropped_thumbnail',

	//cms posters
	'cms/posters' 				=> 'Admin/posters',
	'cms/add_poster' 			=> 'Admin/add_poster',
	'cms/edit_poster/(:any)' 	=> 'Admin/edit_poster/$1',
	'cms/submit_add_poster' 	=> 'Admin/submit_add_poster',
	'cms/submit_edit_poster' 	=> 'Admin/submit_edit_poster',
	'cms/upload_resume'			=> 'Admin/upload_resume',
	'cms/upload_resume2'		=> 'Admin/upload_resume2',
	'cms/delete_poster' 		=> 'Admin/delete_poster',



	'404_override'				=> 'Error_controller',
	'translate_uri_dashes'		=> FALSE,
);
// $route['default_controller'] = 'main';
// $route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;
