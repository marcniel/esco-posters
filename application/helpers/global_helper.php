<?php
if (!function_exists('admin_session_redirection')) {
	function admin_session_redirection(){
		$CI =& get_instance();
		if(!$CI->session->userdata('admin')){
			redirect(base_url().'cms/login', 'location');
		}
	}
}

if (!function_exists('admin_session_val')) {
	function admin_session_val(){
		$CI =& get_instance();
		return $CI->session->userdata('admin');
	}
}

if(!function_exists('upload_image')) {
	function upload_image($data) {
		if(strlen($data['image']) >= 365535){
			return 'Image is too Large';
		}
		$CI =& get_instance();
		$CI->db->where('id', admin_session_val());
		$query = $CI->db->update('cms_users', $data);
		if($query){
			return 1;
		}else{
			return 0;
		}
	}
}



if(!function_exists('get_user_array')){
	function get_user_array($id){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('users');
		$CI->db->where('id',$id);
		$query = $CI->db->get()->result_array();
		$query = current($query);
		if($query){
			return $query;	
		}else{
			return [];
		}
		

	}
}

if(!function_exists('get_array_sub_category')){
	function get_array_sub_category($id){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('sub_category');
		$CI->db->where('cid',$id);
		$query = $CI->db->get()->result_array();
		// $query = current($query);

		if($query){
			return $query;	
		}else{
			return [];
		}

	}
}

if(!function_exists('get_array_usub_category')){
	function get_array_usub_category($id){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('usub_category');
		$CI->db->where('sc_id',$id);
		$query = $CI->db->get()->result_array();
		// $query = current($query);

		if($query){
			return $query;	
		}else{
			return [];
		}
	}
}

if(!function_exists('get_admin_session_array')){
	function get_admin_session_array(){
		$CI =& get_instance();
		$CI->db->select('lname,fname,mname');
		$CI->db->from('cms_users');
		$CI->db->where('id',admin_session_val());
		$query = $CI->db->get()->result_array();
		$query = current($query);

		if($query){
			return $query;	
		}else{
			return [];
		}

	}
}



if(!function_exists('get_user_password')){
	function get_user_password($id){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('users');
		$CI->db->where('id',$id);
		$query = $CI->db->get()->result_array();
		$query = current($query);

		return base64_decode($query['password']);

	}
}

if(!function_exists('get_my_table')){
	function get_my_table($table,$id,$field){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from($table);
		if($table == 'category'){
			$CI->db->where('cid', $id);
		}else if($table == 'sub_category'){
			$CI->db->where('sc_id', $id);
		}else if($table == 'usub_category'){
			$CI->db->where('usc_id', $id);
		}
		$query = $CI->db->get()->result_array();
		$query = current($query);
		if($query){
			return $query[$field];
		}else{
			return '';
		}

	}
}



if(!function_exists('get_sub_category')){
	function get_sub_category($id){
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("sub_category");
		$CI->db->join('category', 'sub_category.cid = category.cid', 'left');
		$CI->db->where("sub_category.cid",$id);
		$query = $CI->db->get()->result_array();
		if ($query) {
			return $query;
		}else{
			return [];
		}
	}
}


if(!function_exists('get_usub_category')){
	function get_usub_category($id){
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("usub_category");
		$CI->db->join('sub_category', 'usub_category.sc_id = sub_category.sc_id', 'inner');
		$CI->db->join('category', 'sub_category.cid = category.cid', 'inner');
		$CI->db->where("usub_category.sc_id",$id);
		$query = $CI->db->get()->result_array();
		if ($query) {
			return $query;
		}else{
			return [];
		}
	}
}

if(!function_exists('get_current_admin_image')) {
	function get_current_admin_image() {
		return get_admin('image');
	}
}

if(!function_exists('get_admin')) {
	function get_admin($field) {
		$CI =& get_instance();

		$CI->db->select('*');
		$CI->db->from('cms_users');
		$CI->db->where('id', admin_session_val());
		$query = $CI->db->get()->result_array();
		$query = current($query);

		if($query){
			return $query[$field];	
		}else{
			return '';
		}
	}
}

if(!function_exists('image_onerror')) {
	function image_onerror() {
		return base_url().'assets/images/esco.jpg';
	}
}

if(!function_exists('sendMail')){
	function sendMail($data){

		$CI =& get_instance();

		$message = $data['message'];

		$config = array(
			// 'useragent' 	=> 'osau.com',
			'protocol' 		=> 'smtp',
			'smtp_host' 	=> 'ssl://smtp.googlemail.com',
			'smtp_port' 	=> 465,
			'smtp_user' 	=> 'escoposter@gmail.com',
			'smtp_pass' 	=> 'esco1234',
			'mailtype'  	=> 'html',
			'wordwrap'  	=> TRUE,
			'charset'   	=> 'iso-8859-1',
			'auth' 			=> TRUE
		);

		$CI->email->set_newline("\r\n");
		$CI->email->initialize($config);
		$CI->email->from('escoposter@gmail.com', 'Esco Posters');
		$CI->email->to( $data['email'] );

		$CI->email->subject($data['subject']);
		$CI->email->message( $message );
		$CI->email->send();
	}
}

if(!function_exists('check_admin_exist')) {
	function check_admin_exist($data) {

		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("cms_users");
		$CI->db->where(array('email'=> $data));
		$result = $CI->db->get();
		return $result->num_rows();
	}
}

if(!function_exists('check_user_count')) {
	function check_user_count() {

		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("users");
		$result = $CI->db->get();
		return $result->num_rows();
	}
}

if(!function_exists('check_poster_count')) {
	function check_poster_count() {

		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("poster");
		$result = $CI->db->get();
		return $result->num_rows();
	}
}

if(!function_exists('check_categories_count')) {
	function check_categories_count() {

		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("category");
		$result = $CI->db->get();
		return $result->num_rows();
	}
}

if(!function_exists('get_data_email')){
	function get_data_email($email,$field){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('cms_users');
		$CI->db->where('email', $email);
		$query = $CI->db->get()->result_array();
		$query = current($query);

		if($query){
			return $query[$field];	
		}else{
			return '';
		}

	}
}

if(!function_exists('unlink_image')){
	function unlink_image($thumbnail){
		$CI =& get_instance();
            $unlink = @unlink(base_url()."/uploads/".$thumbnail);
			if($unlink){
				return 1;
			}else{
				return $unlink;
			}
	}
}

if(!function_exists('current_date')){
	function current_date(){
				return date("Y-m-d");
	}
}


if (!function_exists('timeAgo')) {
	function timeAgo($time){

		// return $time->format('Y-m-d');
		$theDate    = new DateTime($time);
		return $stringDate = $theDate->format('M d, Y');
	}
}

if (!function_exists('get_year')) {
	function get_year(){

		return date("Y");
	}
}

if (!function_exists('getIPAddress')) {
	function getIPAddress() {
	     return $_SERVER['REMOTE_ADDR'];
	}
}

if(!function_exists('get_array_top_viewed_poster')){
		function get_array_top_viewed_poster(){
			$CI =& get_instance();
			$CI->db->select('COUNT(ep_views.view_id) AS view_count, ep_poster.poster_id,ep_poster.poster_name,ep_poster.poster_file, ep_poster.poster_file_thumb');
			$CI->db->from('poster');
			$CI->db->join('views', 'poster.poster_id = views.view_poster', 'left');
			$CI->db->group_by('poster.poster_id');
			$CI->db->order_by('view_count','DESC');
			$CI->db->where('poster.is_downloadable', '1');
			$CI->db->not_like('poster.poster_file', 'pdf');
			$CI->db->limit('3');

			$query = $CI->db->get()->result_array();

			if($query){
				return $query;	
			}else{
				return [];
			}
		}
}
?>
