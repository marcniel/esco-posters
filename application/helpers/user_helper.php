<?php
if (!function_exists('user_session_val')) {
	function user_session_val(){
		$CI =& get_instance();
		return $CI->session->userdata('user');
	}
}

if(!function_exists('get_user_data_email')){
	function get_user_data_email($email,$field){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('users');
		$CI->db->where('email', $email);
		$query = $CI->db->get()->result_array();
		$query = current($query);

		if($query){
			return $query[$field];	
		}else{
			return '';
		}

	}
}

if(!function_exists('get_gmail_by_oauth')){
	function get_gmail_by_oauth($email,$field){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('users');
		$CI->db->where('login_oauth_uid', $email);
		$query = $CI->db->get()->result_array();
		$query = current($query);

		if($query){
			return $query[$field];	
		}else{
			return '';
		}

	}
}

if(!function_exists('get_user_data')){
	function get_user_data($field){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('users');
		$CI->db->where('id', user_session_val());
		$query = $CI->db->get()->result_array();
		$query = current($query);

		return $query[$field];

	}
}

if(!function_exists('get_all_category')){
	function get_all_category(){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('category');
		$query = $CI->db->get()->result_array();

		if($query){
			return $query;	
		}else{
			return [];
		}

	}
}

if(!function_exists('get_sub_by_category')){
	function get_sub_by_category($cid){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('sub_category');
		$CI->db->where(array('cid'=> $cid));
		$query = $CI->db->get()->result_array();

		if($query){
			return $query;	
		}else{
			return [];
		}

	}
}

if(!function_exists('get_sub_by_category_count')){
	function get_sub_by_category_count($cid){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('sub_category');
		$CI->db->where(array('cid'=> $cid));
		$query = $CI->db->get();
		return $query->num_rows();

	}
}

if(!function_exists('get_usub_by_category_count')){
	function get_usub_by_category_count($sc_id){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('usub_category');
		$CI->db->where(array('sc_id'=> $sc_id));
		$query = $CI->db->get();
		return $query->num_rows();

	}
}

if(!function_exists('get_usub_by_category')){
	function get_usub_by_category($sc_id){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('usub_category');
		$CI->db->where(array('sc_id'=> $sc_id));
		$query = $CI->db->get()->result_array();

		if($query){
			return $query;	
		}else{
			return [];
		}

	}
}

if(!function_exists('check_user_exist')) {
	function check_user_exist($data) {

		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("users");
		$CI->db->where(array('email'=> $data));
		$result = $CI->db->get();
		return $result->num_rows();
	}
}

if(!function_exists('user_verified')) {
	function user_verified($data) {

		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("users");
		$CI->db->where(array('id'=> $data,'is_verified' => '1'));
		$result = $CI->db->get();
		return $result->num_rows();
	}
}


if(!function_exists('get_current_user_image')) {
	function get_current_user_image() {
		return get_user_data('image');
	}
}

if(!function_exists('upload_image_user')) {
	function upload_image_user($data) {
		if(strlen($data['image']) >= 365535){
			return 'Image is too Large';
		}
		$CI =& get_instance();
		$CI->db->where('id', user_session_val());
		$query = $CI->db->update('users', $data);
		if($query){
			return 1;
		}else{
			return 0;
		}
	}
}

if(!function_exists('get_array_poster_select')){
	function get_array_poster_select(){
		$CI =& get_instance();
		$CI->db->select('poster_id,poster_name');
		$CI->db->from('poster');
		$CI->db->where('is_downloadable', '1');
		$query = $CI->db->get()->result_array();
		return $query;
	}
}

if(!function_exists('get_array_poster')){
	function get_array_poster($category,$subcat,$usubcat,$img_type,$search,$downloadable,$sort,$limit=null,$offset=null){
		$CI =& get_instance();

		$CI->db->select('poster.*, COUNT(ep_views.view_id) AS view_count , COUNT(ep_download.download_id) AS dl_count');
		$CI->db->from('poster');
		$CI->db->join('views', 'poster.poster_id = views.view_poster', 'left');
		$CI->db->join('download', 'poster.poster_id = download.poster_id', 'left');
		$CI->db->group_by('poster.poster_id');

		$CI->db->where('poster.poster_id>', '0');
		if($category != ''){
			$CI->db->where('poster.category_id', $category);
		}
		if($subcat != ''){
			$CI->db->where('poster.sub_category_id', $subcat);
		}

		if($usubcat != ''){
			$CI->db->where('poster.usub_category_id', $usubcat);
		}

		if($downloadable != ''){
			if($downloadable == '1'){
				$CI->db->where('poster.is_downloadable', '1');
			}else{
				$CI->db->where('poster.is_downloadable', '0');
			}

		}

		if($img_type != ''){
			if($img_type == 'pdf'){
				$CI->db->where('poster.poster_file LIKE', '%'.$img_type.'%');
			}else{
				$CI->db->not_like('poster.poster_file', 'pdf');
			}
		}

		if($search != ''){
			$CI->db->where('poster.poster_name LIKE', '%'.$search.'%');
		}

		if($sort != ''){
			if($sort == 'views'){
				$CI->db->order_by('view_count','DESC');
			} else if($sort == 'dl'){
				$CI->db->order_by('dl_count','DESC');
			}
		} else {
			$CI->db->order_by('poster.poster_name', 'ASC');
		}
		$CI->db->limit($limit,$offset);
		$query = $CI->db->get()->result_array();
		// $query = current($query);

		if($query){
			return $query;	
		}else{
			return [];
		}

	}
}

if(!function_exists('add_views')) {
	function add_views($params = array()) {
		if($params['view_by'] == ''){
			$params['view_by'] = getIPAddress();
		}
		$CI =& get_instance();
			$data = array(
			'view_by' 			=> $params['view_by'],
			'view_poster' 		=> $params['view_poster'],
			'view_date' 		=> current_date()
				);
		if(check_view($params['view_by'], $params['view_poster'],current_date()) > 0){

		}else{
			$CI->db->insert('views', $data);
		}
	}
}

if(!function_exists('check_view')){
	function check_view($id,$poster,$date){
		// if($id == 0){
		// 	return 0;
		// }
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('views');
		$CI->db->where(array('view_by'=> $id,'view_poster' => $poster,'view_date' => $date));
		$query = $CI->db->get();
		return $query->num_rows();

	}
}

if(!function_exists('poster_view_count')){
	function poster_view_count($poster_id){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('views');
		$CI->db->where(array('view_poster'=> $poster_id));
		$query = $CI->db->get();
		return $query->num_rows();

	}
}

if(!function_exists('poster_download_count')){
	function poster_download_count($poster_id){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('download');
		$CI->db->where(array('poster_id'=> $poster_id));
		$query = $CI->db->get();
		return $query->num_rows();

	}
}

if(!function_exists('get_report_array')){
	function get_report_array($id){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('reports');
		$CI->db->where('report_id',$id);
		$query = $CI->db->get()->result_array();
		$query = current($query);
		if($query){
			return $query;	
		}else{
			return [];
		}
		

	}
}
?>
