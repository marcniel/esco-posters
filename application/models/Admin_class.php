<?php

class Admin_class extends CI_Model
{

	public function users_login($data){
		$this->db->select('*');
		$this->db->from('cms_users');
		$this->db->where(array('email'=>$data['email'],'password'=>base64_encode($data['password'])));
		$query = $this->db->get()->result_array();
		$query = current($query);
		if($query > 0){

			return $query['id'];
		}else{
			return 0;
		}
	}


	public function get_users(){
		$this->db->select("*");
		$this->db->from("users");
		$query = $this->db->get()->result_array();
		if ($query) {
			return $query;
		}else{
			return $query;
		}
	}

	public function get_downloads(){
		$this->db->select("*");
		$this->db->from("download");
		$this->db->join('users', 'download.get_by = users.id', 'inner');
		$this->db->join('poster', 'download.poster_id = poster.poster_id', 'left');
		$query = $this->db->get()->result_array();
		if ($query) {
			return $query;
		}else{
			return $query;
		}
	}

	public function get_reports(){
		$this->db->select("*");
		$this->db->from("reports");
		$query = $this->db->get()->result_array();
		if ($query) {
			return $query;
		}else{
			return $query;
		}
	}


	public function get_posters(){
		$this->db->select("*");
		$this->db->from("poster");
		$this->db->join('category', 'poster.category_id = category.cid', 'left');
		$this->db->join('sub_category', 'poster.sub_category_id = sub_category.sc_id', 'left');
		$this->db->join('usub_category', 'poster.usub_category_id = usub_category.usc_id', 'left');
		$query = $this->db->get()->result_array();
			return $query;

	}

	public function submit_add_poster($post){
			$data = array(
				'poster_file'		=> $post['poster_file'],
				'poster_file_thumb'	=> $post['poster_file_thumb'],
				'poster_by'			=> $post['poster_by'],
				'poster_name'		=> $post['poster_name'],
				'poster_content'	=> $post['poster_content'],
				'category_id'		=> $post['category_id'],
				'sub_category_id'	=> $post['sub_category_id'],
				'usub_category_id'	=> $post['usub_category_id'],
				'is_downloadable'	=> $post['is_downloadable'],
				'posted_date'		=> $post['posted_date'],
				'created_at' 		=> current_date(),
				'published_by_id' 	=> admin_session_val(),
			);
			$result = $this->db->insert('poster', $data);
			if ($result) {
				return 1;
			}else{
				return 0;
			}
	}

	public function submit_edit_poster($post){
			$data = array(
				'poster_file'		=> $post['poster_file'],
				'poster_file_thumb'	=> $post['poster_file_thumb'],
				'poster_by'			=> $post['poster_by'],
				'poster_name'		=> $post['poster_name'],
				'poster_content'	=> $post['poster_content'],
				'category_id'		=> $post['category_id'],
				'sub_category_id'	=> $post['sub_category_id'],
				'usub_category_id'	=> $post['usub_category_id'],
				'is_downloadable'	=> $post['is_downloadable'],
				'posted_date'		=> $post['posted_date'],
				'date_updated' => current_date(),
			);
			$this->db->where('poster_id',$post['id']);
			$result = $this->db->update('poster', $data);
			if ($result) {
				return 1;
			}else{
				return 0;
			}
	}

	public function delete_poster($params){
			$data = array(
				'poster_id' 	=> $params['id'],
			);
			$this->db->where($data);
			$result = $this->db->delete('poster');
			if ($result) {
				return 1;

			}else{
				return 0;
			}
	}

	public function add_user($post){
			$data = array(
				'fname' 			=> $post['fname'],
				'mname' 			=> $post['mname'],
				'lname' 			=> $post['lname'],
				'number' 			=> $post['number'],
				'email' 			=> $post['email'],
				'password' 			=> base64_encode($post['password']),
				'verification_key' 	=> $post['verification_key'],
				'is_verified' 		=> $post['is_verified'],
				'date_added' => current_date(),
			);
			if(check_user_exist($post['email']) >= 1){
	      return 2;
	    } else {
				$result = $this->db->insert('users', $data);
				if ($result) {
					return 1;
				}else{
					return 0;
				}
			}
	}

	public function edit_user($post){
			$data = array(
				'fname' 			=> $post['fname'],
				'mname' 			=> $post['mname'],
				'lname' 			=> $post['lname'],
				'number' 			=> $post['number'],
				'email' 			=> $post['email'],
				'password' 			=> base64_encode($post['password']),
				'is_verified' 		=> '1',
				'date_updated' => current_date(),
			);

			if(check_user_exist($post['email']) >= 1 && $post['email'] != $post['old_email']){
	      return 2;
	    } else {
				$this->db->where('id',$post['id']);
				$result = $this->db->update('users', $data);
				if ($result) {
					return 1;
				}else{
					return 0;
				}
			}
	}

	public function get_posters_edit($post){
			$this->db->select("*");
			$this->db->from("poster");
			$this->db->where('poster_id',$post);
			$query = $this->db->get()->result_array();
			$query = current($query);
			// if ($query) {
			// 	return 1;
			// }else{
			// 	return 0;
			// }
			return $query;
	}

	public function delete_user($params){
			$data = array(
				'id' 	=> $params['id'],
			);
			$this->db->where($data);
			$result = $this->db->delete('users');
			if ($result) {
				return 1;

			}else{
				return 0;
			}
	}

	public function get_category(){
		$this->db->select("*");
		$this->db->from("category");
		$query = $this->db->get()->result_array();
		if ($query) {
			return $query;
		}else{
			return $query;
		}
	}

	public function add_category($params){
			$data = array(
				'name' 	=> $params['category'],
			);
			$result = $this->db->insert('category', $data);
			if ($result) {
				return 1;
			}else{
				return 0;
			}
	}

	public function edit_category($params){
			$data = array(
				'name' 	=> $params['category'],
			);
			$data2 = array(
				'cid' 	=> $params['id'],
			);
			$this->db->where($data2);
			$result = $this->db->update('category', $data);
			if ($result) {
				return 1;
			}else{
				return 0;
			}
	}

	public function delcategory($params){
			$data = array(
				'cid' 	=> $params['id'],
			);
			$this->db->where($data);
			$result = $this->db->delete('category');
			if ($result) {
				$data = array(
					'cid' 	=> $params['id'],
				);
				$this->db->where($data);
				$result2 = $this->db->delete('sub_category');
				// $result3 = $this->Admin_class->delete_ucategory($params['id']);

				if($result){
						return 1;
					}else{
						return 0;
					}
			}else{
				return 0;
			}
	}

	// public function delete_ucategory($idddd){
	// 	$this->db->select("*");
	// 	$this->db->from("sub_category");
	// 	$this->db->where('cid',$idddd);
	// 	$query = $this->db->get()->result_array();
	// 	if ($query) {
	// 		foreach ($query as $key => $value) {
	// 			$data = array(
	// 				'sc_id' 	=> $value['sc_id'],
	// 			);
	// 			$this->db->where($data);
	// 			$this->db->delete('usub_category');
	// 		}
	// 		return 1;
	// 	}else{
	// 		return 0;
	// 	}
	// }


	public function get_sub_category(){
		$this->db->select("*");
		$this->db->from("sub_category");
		$this->db->join('category', 'sub_category.cid = category.cid', 'left');
		$query = $this->db->get()->result_array();
		if ($query) {
			return $query;
		}else{
			return $query;
		}
	}

	public function get_usub_category(){
		$this->db->select("*");
		$this->db->from("usub_category");
		$this->db->join('sub_category', 'usub_category.sc_id = sub_category.sc_id', 'inner');
		$this->db->join('category', 'sub_category.cid = category.cid', 'inner');
		$query = $this->db->get()->result_array();
		if ($query) {
			return $query;
		}else{
			return $query;
		}
	}



	public function add_sub_category($params){
			$data = array(
				'cid' 			=> $params['category'],
				'sname'			=> $params['sub_category'],
			);
			$result = $this->db->insert('sub_category', $data);
			if ($result) {
				return 1;
			}else{
				return 0;
			}
	}


	public function add_usub_category($params){
			$cid = get_my_table('sub_category',$params['sub_category'],'cid');
			$data = array(
				//'cid'				=> $cid,
				'sc_id' 			=> $params['sub_category'],
				'usname'			=> $params['usub_category'],
			);
			$result = $this->db->insert('usub_category', $data);
			if ($result) {
				return 1;
			}else{
				return 0;
			}
	}

	public function editsubcategory($params){
			$data = array(
				'cid' 			=> $params['category'],
				'sname'			=> $params['sub_category'],
			);
			$data2 = array(
				'sc_id' 	=> $params['id'],
			);
			$this->db->where($data2);
			$result = $this->db->update('sub_category', $data);
			if ($result) {
				return 1;
			}else{
				return 0;
			}
	}

	public function edit_usub_category($params){
			$data = array(
				'usname'		=> $params['usub_category'],
				'sc_id'			=> $params['sub_category'],
			);
			$data2 = array(
				'usc_id' 	=> $params['id'],
			);
			$this->db->where($data2);
			$result = $this->db->update('usub_category', $data);
			if ($result) {
				return 1;
			}else{
				return 0;
			}
	}

	public function delete_sub_category($params){
			$data = array(
				'sc_id' 	=> $params['id'],
			);
			$this->db->where($data);
			$result = $this->db->delete('sub_category');
			if ($result) {
				$data = array(
					'sc_id' 	=> $params['id'],
				);
				$this->db->where($data);
				$result2 = $this->db->delete('usub_category');
				if($result2){
					return 1;
				}


			}else{
				return 0;
			}
	}

	public function delete_usub_category($params){
			$data = array(
				'usc_id' 	=> $params['id'],
			);
			$this->db->where($data);
			$result = $this->db->delete('usub_category');
			if ($result) {
				return 1;

			}else{
				return 0;
			}
	}

	public function admin_profile($post){
		$data = array(
			'lname' 	=> $post['lname'],
			'fname'		=> $post['fname'],
			'mname' 	=> $post['mname'],
			'email'		=> $post['email'],
			'password' 	=> base64_encode($post['password']),
		);
		$this->db->where('id', admin_session_val());
		$query = $this->db->update('cms_users', $data);

		if($query){
			return 1;// Success Message
		} else {
			return $query;// Failed Message
		}
	}

	public function admin_forget_password($post){
		if(check_admin_exist($post['email']) >= 1){
			$message = 'Hello admin ';
			$message .= get_data_email($post['email'],'fname').' '.get_data_email($post['email'],'mname').' '.get_data_email($post['email'],'lname');
			$message .= ' from ESCO Posters your password is: '.base64_decode(get_data_email($post['email'],'password'));


			$maildata = array(
			'message' =>  $message,
			'email'	  => $post['email'],
			'subject' => 'Forget Password',
			);
			sendMail($maildata);
			return 1;
		}else{
			return 0;
		}
	}

	public function send_email($post){
			$maildata = array(
			'message' =>  $post['email_content'],
			'email'	  => $post['email'],
			'subject' => $post['subject'],
			);
			sendMail($maildata);
			return 1;
	}

	public function get_poster_counts($limit=null){
		if ($limit) {
			$limit = ' LIMIT ' . $limit;
		}else{
			$limit = '';
		}
		$query = $this->db->query('SELECT created_at, COUNT(poster_id) AS poster_count, MONTH(created_at) AS monthly, YEAR(created_at) AS yearly FROM ep_poster WHERE YEAR(created_at) = '.date("Y").'
		GROUP BY YEAR(created_at) ,MONTH(created_at) ORDER BY YEAR(created_at) asc, MONTH(created_at) asc' . $limit);

		return $query->result_array();
	}
}

?>
