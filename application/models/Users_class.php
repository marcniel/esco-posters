<?php

class Users_class extends CI_Model
{

  public function sign_up($post){
    $data = array(
      'fname' 			=> $post['fname'],
      'mname' 			=> $post['mname'],
      'lname' 			=> $post['lname'],
      'number' 			=> $post['number'],
      'email' 			=> $post['email'],
      'password' 			=> base64_encode($post['password']),
      'verification_key' 	=> $post['verification_key'],
      'is_verified' 		=> $post['is_verified'],
      'date_added'  => current_date(),
    );
    if(check_user_exist($post['email']) >= 1){
      return 2;
    } else {
      $result = $this->db->insert('users', $data);
      if ($result) {
        $message = 'Hello '.$post['fname'].' '.$post['lname'];
  			$message .= ' Please verify your account. ';
  			$message .= 'Your verification code is '.$post['verification_key'].'.';

        $maildata = array(
  			'message' =>  $message,
  			'email'	  => $post['email'],
  			'subject' => 'Esco Poster: Please verify your account.',
  			);

        sendMail($maildata);

        return 1;
      }else{
        return 0;
      }
    }
  }

  public function login_user($data){
    $this->db->select('*');
    $this->db->from('users');
    $this->db->where(array('email'=>$data['email'],'password'=>base64_encode($data['password'])));
    $query = $this->db->get()->result_array();
    $query = current($query);
    if($query > 0){
      return $query['id'];
    }else{
      return 0;
    }
  }


  public function users_forget_password($post){
    if(check_user_exist($post['email']) >= 1){
      $message = 'Hello ';
      $message .= get_user_data_email($post['email'],'fname').' '.get_user_data_email($post['email'],'mname').' '.get_user_data_email($post['email'],'lname');
      $message .= ' from ESCO Posters your password is: '.base64_decode(get_user_data_email($post['email'],'password'));


      $maildata = array(
      'message' =>  $message,
      'email'   => $post['email'],
      'subject' => 'Forget Password',
      );
      sendMail($maildata);
      return 1;
    }else{
      return 0;
    }
  }


  public function send_sms(){
      $message = 'Hello ';
      $message .= get_user_data('fname').' '.get_user_data('mname').' '.get_user_data('lname');
      $message .= ' from ESCO Posters your verification key is: '.get_user_data('verification_key');


      $maildata = array(
      'message' =>  $message,
      'email'   => get_user_data('email'),
      'subject' => 'Verrification Key',
      );
      sendMail($maildata);
      return 1;
  }

  public function user_profile($post){
    $data = array(
      'lname'   => $post['lname'],
      'fname'   => $post['fname'],
      'mname'   => $post['mname'],
      'email'   => $post['email'],
      'number'  => $post['number'],
      'password'  => base64_encode($post['password']),
      'date_updated' => current_date(),
    );
    $this->db->where('id', user_session_val());
    $query = $this->db->update('users', $data);

    if($query){
      return 1;// Success Message
    } else {
      return $query;// Failed Message
    }
  }

  public function verify_user(){
      $data = array(
        'is_verified'      => '1',
      );
      $this->db->where('id',user_session_val());
      $result = $this->db->update('users', $data);
      if ($result) {
        return 1;
      }else{
        return 0;
      }
  }

  public function get_posters_id($post){
      $this->db->select("*");
      $this->db->from("poster");
      $this->db->where('poster_id',$post);
      $query = $this->db->get()->result_array();
      $query = current($query);
      // if ($query) {
      //  return 1;
      // }else{
      //  return 0;
      // }
      return $query;
  }

  public function get_posters_name(){
      // $this->db->select("poster_name");
      // $this->db->from("poster");
      $query = $this->db->query('
      SELECT `poster_name` FROM ep_poster
    ');
      // $query = $this->db->get()->result_array();
      foreach ($query->result_array() as $row){
          $new_row= $row['poster_name'];
          $row_set[] = $new_row;
      }
      return $row_set;
  }

  

  public function poster_download($post){
      $data = array(
        'poster_id'     => $post['poster_id'],
        'get_by'        => $post['get_by'],
        'date_get'      => current_date(),
      );
      $result = $this->db->insert('download', $data);
      if ($result) {
        return 1;
      }else{
        return 0;
      }
  }

  public function send_report($post){
      $data = array(
        'fname'       => $post['fname'],
        'lname'       => $post['lname'],
        'subject'     => $post['subject'],
        'email'       => $post['email'],
        'content'     => $post['content'],
      );
      $result = $this->db->insert('reports', $data);
      if ($result) {
        return 1;
      }else{
        return 0;
      }
  }


}  

 ?>
