<div class="modal fade" id="signup" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Sign Up</h4>
      </div>
    <form id="sign_up">
      <div class="modal-body">

          <div class="form-group">
            <label for="fname">First Name:</label>
            <input type="text" class="form-control" id="fname" name="fname" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="mname">Middle Name:</label>
            <input type="text" class="form-control" id="mname" name="mname" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="lname">Last Name:</label>
            <input type="text" class="form-control" id="lname" name="lname" autocomplete="off" required>
          </div>

          <div class="form-group">
            <label for="number">Phone Number:</label>
            <input type="number" class="form-control" id="number" name="number" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="email" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" id="password" name="password" required>
            <p style="color:black;"><input type="checkbox" onclick="login_show_pass()"> Show Password</p>
          </div>

          <div class="form-group">
            <label for="cpassword">Confirm Password:</label>
            <input class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" type="password" id="cpassword" name="cpassword">
            <p style="color:black;"><input type="checkbox" onclick="login_show_cpass()"> Show Password</p>
          </div>

          <div class="form-group" style="display: none;">
            <label for="verification_key">Verification key:</label>
              <input type="text" class="form-control" id="verification_key" value="<?php echo rand(); ?>" readonly>
              <input type="hidden" id="is_verified" value="0">
          </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </form>
    </div>
    <div class="bottomLinkMenu">
      <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#login">Login</a>
      <span class="separator">•</span>
      <a href="#">Sign Up</a>
      <span class="separator">•</span>
      <a href="#" data-dismiss="modal">Close</a>
    </div>
    </div>
  </div>


<div class="modal fade" id="login" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Login Form</h4>
      </div>
    <form id="login_user">
      <div class="modal-body">
          <?php if($page == 'home' || $page == 'posters' || $page == 'poster_view'){
            echo '<div align="center">'.$login_button . '</div>';
          } ?>
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="email" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password2" name="password2" required>
            <p style="color:black;"><input type="checkbox" onclick="login_show_pass2()"> Show Password</p>
          </div>
          <div class="flex-sb-m w-full p-b-30">
            
            <div>
              <a href="#" data-toggle="modal" data-target="#forget_password" >
                Forgot Password?
              </a>
            </div>
          </div>


        
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </form>
    </div>
    <div class="bottomLinkMenu">
      <a href="#">Login</a>
      <span class="separator">•</span>
      <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#signup">Sign Up</a>
      <span class="separator">•</span>
      <a href="#" data-dismiss="modal">Close</a></div>
  </div>
</div>


<div class="modal fade" id="forget_password" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Forgot password?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="forgotpassword2">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="email">Please put your email</label>
              <input type="email" class="form-control" id="email" name="email" autocomplete="off">
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>

  </body>
  <div style="margin-top: 5vh;"></div>
  <div class="footer footer_div">
    <div>&copy; <?php echo get_year(); ?> Esco Posters. All Rights Reserved</div>
  </div>
</html>

<script>
	function login_show_pass() {
      var x = document.getElementById("password");

      if (x.type === "password") {
      	x.type = "text";
      } else {
      	x.type = "password";
      }
    }

  function login_show_cpass() {
      var x = document.getElementById("cpassword");

      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }

    function login_show_pass2() {
      var x = document.getElementById("password2");

      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
</script>
