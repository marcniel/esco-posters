<html>
  <head>
    <title><?php echo $title ?></title>
    <script>
      var base_url = "<?php echo base_url() ?>";
      var page = "<?php echo $page; ?>";
    </script>
    
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/downloaded/bootstrap.min.css">
    <script src="<?php echo base_url(); ?>assets/plugin/downloaded/3.5.1jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugin/downloaded/bootstrap.min.js"></script>
    <?php if($page == 'home'){
      echo '<link rel="stylesheet" href="'.base_url().'assets/plugin/bootstrap/old/css/bootstrap.css">';  
    } ?>
    

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/app.css">


  <!-- for jquery -->
  <script src="<?php echo base_url(); ?>assets/plugin/downloaded/171jquery.min.js" type="text/javascript"></script>
  <link href="<?php echo base_url(); ?>assets/plugin/downloaded/familygoogleapicss.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/downloaded/jquery-confirm.min.css">
    <script src="<?php echo base_url(); ?>assets/plugin/downloaded/jquery-confirm.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/fontawesome/font-awesome.min.css">
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/website/js/app.js"></script>
  <?php if($page == 'home' || $page == 'posters'){
    echo '
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">';
  } ?>
  <!-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> -->
  </head>
  <body>
    <div class="header">
      <div class="navbar-brand">
        <a class="navbar-brand-image-link" href="<?php echo base_url(); ?>">
          <img src="<?php echo base_url(); ?>assets/images/esco-logo.png" alt="Esco" class="img-responsive img-ie">
        </a>
        <ul class="navbar-right">
          <li class="display-flex-center">
            <a href="<?php echo base_url().'posters' ?>" class="poster_class <?php echo ($page == 'posters' || $page == 'poster_view')? 'active' : ''; ?>" id="LoginRegister"> Posters</a>
            <?php if (user_session_val() == ''){ ?>
              <a href="#" id="LoginRegister" class="register_user" data-toggle="modal" data-target="#login"> <i class="fa fa-user"></i> Login/Register</a>
            <?php }else{
              if(user_verified(user_session_val()) == 0){
                if($page != 'verification'){
                  redirect(base_url().'verification', 'location');
                }
              }
            ?>
              <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle top-img-button" type="button" data-toggle="dropdown">
                  <img src="<?php echo get_current_user_image(); ?>" onerror="this.src='<?php echo image_onerror() ?>';" id="image_view" class="top-img-profile" height="50" width="50">
                </button>
                <ul class="dropdown-menu">
                  <?php if(user_verified(user_session_val()) == 1){?>
                  <!-- <li><a href="#"></a></li>
                  <li><a href="#">CSS</a></li> -->
                  <li><a href="<?php echo base_url().'profile' ?>" class="<?php echo ($page == 'profile')? 'active' : ''; ?>">
                  <?php
                    echo (get_user_data('fname') != '')? get_user_data('fname').' ' : '' ;
                    echo (get_user_data('mname') != '')? get_user_data('mname').' ' : '' ;
                    echo (get_user_data('lname') != '')? get_user_data('lname').' ' : '' ;
                  ?>
                  </a></li>
                  <?php } ?>
                  <li><a href="<?php echo base_url().'Users/logout' ?>">Logout</a></li>
                </ul>
              </div>
            <?php } ?>
          </li>
        </ul>
        <ul class="navbar-right-responsive">
        <li class="dropdown">
          <a class="dropdown-toggle navbar-right-expander" data-toggle="dropdown" href="#">
            <i class="fa fa-bars fa-lg white"></i>
          </a>

          <ul class="dropdown-menu navbar-right-expander-menu">
            <li><a href="<?php echo base_url().'posters' ?>" class="menu-link <?php echo ($page == 'posters' || $page == 'poster_view')? 'active' : ''; ?>"> Posters</a>
            <?php if (user_session_val() == ''){ ?>
              <li><a href="#" class="menu-link" id="LoginRegister" data-toggle="modal" data-target="#login">Login/Register</a></li>
            <?php }else{ ?>
              <?php if(user_verified(user_session_val()) == 1){?>
                <li><a class="menu-link <?php echo ($page == 'profile')? 'active' : ''; ?>" href="<?php echo base_url().'profile' ?>">
                  <?php
                    echo (get_user_data('fname') != '')? get_user_data('fname').' ' : '' ;
                    echo (get_user_data('mname') != '')? get_user_data('mname').' ' : '' ;
                    echo (get_user_data('lname') != '')? get_user_data('lname').' ' : '' ;
                    echo ' - Profile'
                  ?>
                </a></li>
              <?php } ?>
              <li><a class="menu-link" href="<?php echo base_url().'Users/logout' ?>">Logout</a></li>
            <?php } ?>
          </ul>
        </li>
        </ul>
      </div>

    </div>
