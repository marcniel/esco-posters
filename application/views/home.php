
<div class="home" id="search_container">
	<div class="divcontainer">
		<h1 class="search_poster_logo">Esco Posters</h1>
		<form action = "<?php echo base_url().'posters' ?>" method="" class="search_poster">

			<input type="text" id="suggest_item" name = "q" placeholder="Search poster name" class="form-control search-textbox">
			<button><i class="fa fa-search" aria-hidden="true"></i></button>
		</form>
	</div>
	<div id="carousel_container" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel_container" data-slide-to="0" class="active"></li>
      <li data-target="#carousel_container" data-slide-to="1"></li>
      <li data-target="#carousel_container" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
			<?php
			$poster_count = 1;
			foreach (get_array_top_viewed_poster() as $key => $value) {
			$file_type = pathinfo($value['poster_file'], PATHINFO_EXTENSION);

			echo '<div class="item ';
			if ($poster_count == 1){
				echo'active';
			}
			echo '">';
				echo'<div class="item-inside">';
					echo '<a href = "'.base_url().'uploading/'.$value['poster_id'].'" >'; ?>
						<img src="<?php echo base_url().'uploads/'.$value['poster_file'] ?>" alt="'. $value['poster_name'].'" style="width:100%;" onerror="this.src='<?php echo base_url().'assets/images/no_image.jpg' ?>';">
				<?php	echo '</a>';
				echo '</div>';
			echo '</div>';

			$poster_count++;
		}
		?>
      <!-- <div class="item active">
        <img src="la.jpg" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="chicago.jpg" alt="Chicago" style="width:100%;">
      </div>

      <div class="item">
        <img src="ny.jpg" alt="New york" style="width:100%;">
      </div> -->
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#carousel_container" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel_container" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
	<div class="divcontainer_contact">
		<form id="contact_us_form">
			<h2>Contact Us</h2>
			<div class="form-group">
				<input type="text" name = "fname" id="fname" placeholder="First name" class="form-control" autocomplete="off" required="">
			</div>
			<div class="form-group">
				<input type="text" name = "lname" id="lname" placeholder="Last name" class="form-control" autocomplete="off" required="">
			</div>
			<div class="form-group">
				<input type="text" name = "subject" id="subject" placeholder="Subject" class="form-control" autocomplete="off" required="">
			</div>
			<div class="form-group">
				<input type="email" name = "email" id="email" placeholder="Email" class="form-control" autocomplete="off" required="">
			</div>
			<div class="form-group">
				<textarea cols="50" rows="4" name = "content" id="content" placeholder="Write your message here" class="form-control" autocomplete="off" required=""></textarea>
			</div>
			<button type="submit" class="btn btn-success">Send Message</button>
		</form>
	</div>
</div>
