<div class="home" id="search_container">
	<div class="div_poster overflow_auto work-break-break-word">
		<div class="pull-right font-30"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;<?php echo poster_view_count($get_poster['poster_id']); ?>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-download" aria-hidden="true"></i>&nbsp;<?php echo poster_download_count($get_poster['poster_id']); ?></div>
		<h1><?=$get_poster['poster_name']?></h1>
		<h6><?=timeAgo($get_poster['posted_date'])?></h6>
		<h4>Author: <?=$get_poster['poster_by'] ?></h4>
		<center>
		<?php $file_type = pathinfo($get_poster['poster_file'], PATHINFO_EXTENSION);
		if($file_type != 'pdf' && $file_type != 'PDF'){ ?>
			<img src="<?php echo base_url().'uploads/'.$get_poster['poster_file']; ?>" onerror="this.src='<?php echo base_url().'assets/images/no_image.jpg' ?>';" class="poster_view_photo" draggable="false">
		
		<?php }else{ ?>

			<img src="<?php echo base_url().'uploads/'.$get_poster['poster_file_thumb']; ?>" onerror="this.src='<?php echo base_url().'assets/images/pdf.png' ?>';" class="poster_view_photo" draggable="false">

		<?php } ?>
		<div class="pull-center">
			<?php
			if (user_session_val() != ''){ 
				if($get_poster['is_downloadable'] == '1'){
					$file_type = pathinfo($get_poster['poster_file'], PATHINFO_EXTENSION);
					if($file_type != 'pdf' && $file_type != 'PDF'){
						if(file_exists('uploads/'.$get_poster['poster_file']) == '1'){
							echo '<a class="btn btn-info btn-lg" onclick="save_download('.$get_poster['poster_id'].','.user_session_val().');" download = "'.$get_poster['poster_name'].'.'.$file_type.'" href = "'.base_url().'uploads/'.$get_poster['poster_file'].'">GET POSTER</a>';
						}
				 	}else{
				 		if(file_exists('uploads/'.$get_poster['poster_file']) == '1'){
				 			echo '<a href = "'.base_url().'uploads/'.$get_poster['poster_file'].'" onclick="save_download('.$get_poster['poster_id'].','.user_session_val().');" class="btn btn-info btn-lg" download = "'.$get_poster['poster_name'].'.'.$file_type.'" target="_blank">GET POSTER</a>';
				 		}
					}
				}
			?>
			
			<?php } ?>
		</div>
		<div style="margin-top: 20px;"></div>
		</center>
		<div class="div_container_poster">
			<?=$get_poster['poster_content']?>
			<div>
				<?php 
				echo (get_my_table('category',$get_poster['category_id'],'name') != '')? '<div class="div_tag_content"># '.get_my_table('category',$get_poster['category_id'],'name').'</div>' : '';
				echo (get_my_table('sub_category',$get_poster['sub_category_id'],'sname') != '')?'<div class="div_tag_content"># '.get_my_table('sub_category',$get_poster['sub_category_id'],'sname').'</div>' : '';
				echo (get_my_table('usub_category',$get_poster['usub_category_id'],'usname') != '')? '<div class="div_tag_content"># '.get_my_table('usub_category',$get_poster['usub_category_id'],'usname').'</div>' : '';
				?>
			</div>
		</div>

	</div>
</div>
<script>
	function save_download(poster,user){
		var data = {'poster_id':poster,'get_by':user};
			$.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+'users/poster_download',
                data:data,
                success:function(data)
                {
            		if(data == 0){
            			app.alert('Error','There`s something wrong');
            		}
                }
            });
	}
</script>