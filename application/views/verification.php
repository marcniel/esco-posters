<div class="home" id="search_container">
<form id="verify_user">
	<div class="form-group">
		<h2><b>Instruction: </b>Please reload the page if verification key does not send to your email</h2>
	    <label for="vkey">Enter your verification key:</label>
	    <input type="text" class="form-control" name="vkey" id="vkey" autocomplete="off" required>
	    <input type = "hidden" name = "hvkey" id="hvkey" value = "<?php echo get_user_data('verification_key') ?>">
	</div>
	<button type="submit" class="btn btn-success" style="float:right;">Submit</button>
</form>
</div>
