<center>
  <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
  <form class="nodisplay" id="uploadForm" action="<?php echo base_url();?>cms/upload_resume" method="POST" enctype="multipart/form-data">
    <input type="file" name ="file" id="file">
  </form>

  <form class="nodisplay" id="uploadForm2" action="<?php echo base_url();?>cms/upload_resume2" method="POST" enctype="multipart/form-data">
    <input type="file" name ="file2" id="file2">
  </form>

  <form id="add_poster">
    <input type="hidden" id="poster_file">
    <input type="hidden" id="poster_file_thumb">
    <button type="submit" class="btn btn-success" style="float:right;">Submit</button><br>
    <div class="form_poster">
    
    <div class="cover_upload" id="upload_file"><i class="fa fa-upload upload_fa" aria-hidden="true"></i></div>
    
    <div class="cover_upload2 display-flex" id="" style="display: none;">
      <img style="width:auto;height:300px;" src=""><div id="close_file"><i class="fa fa-close " ></i></div>
    </div>

    <div class="cover_upload3 display-flex" id="" style="display: none;">
      <embed style="width:auto;height:300px;" src=""></embed><div id="close_file2"><i class="fa fa-close " ></i></div>
    </div>
  <br>
    <div class="cover_upload4" id="upload_file2" style="display: none;"><i class="fa fa-upload upload_fa" aria-hidden="true"></i> <br>Upload pdf thumbnail (required for pdf)</div>

    <div class="cover_upload5 display-flex" id="" style="display: none;">
      <img style="width:auto;height:300px;" src=""><div id="close_file3"><i class="fa fa-close " ></i></div>
    </div>

    <div class="form-group">
      <label for="poster_title">Poster Title:</label>
      <input type="text" class="form-control" name="poster_title" id="poster_title" autocomplete="off" required>
    </div>

    <div class="form-group">
      <label for="poster_by">Author:</label>
      <div style="display: flex;">
        <input type="text" class="form-control" name="poster_by" id="poster_by" autocomplete="off" required><a id="get_my_name">Click to get your name</a>
      </div>
    </div>

    <div class="form-group">
      <label for="poster_content">Poster Content:</label>
      <textarea class="form-control" name="poster_content" id="poster_content" autocomplete="off" required></textarea>
    </div>
    <script>
      CKEDITOR.replace( 'poster_content' );
    </script>


    <div class="form-group">
      <label for="category">Category:</label>
      <select class="form-control" name="category" id="category" autocomplete="off" required>
        <option value=''></option>
        <?php 
          foreach ($category as $key => $value) {
        ?>
            <option value="<?=$value['cid']?>"><?=$value['name']?></option>
        <?php
          }
        ?>
      </select>
    </div>

    <div class="form-group">
      <label for="sub_category">Sub Category:</label>
      <select class="form-control" name="sub_category" id="sub_category">
        
      </select>
    </div>

    <div class="form-group">
      <label for="usub_category">Under Sub Category:</label>
      <select class="form-control" name="usub_category" id="usub_category">
      </select>
    </div>

    <div class="form-group">
      <label for="is_downloadable">Downloadable?</label>
      <select class="form-control" name="is_downloadable" id="is_downloadable">
        <option value = '1'>Yes</option>
        <option value = '0'>No</option>
      </select>
    </div>

    <div class="form-group">
      <label for="posted_date">Posted Date:</label>
      <input type="date" max = "<?php echo current_date(); ?>" class="form-control" name="posted_date" id="posted_date" autocomplete="off" required>
    </div>

  </div>
  </form>
</center>