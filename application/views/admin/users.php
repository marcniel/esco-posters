    <button type="button" class="btn btn-info btn-sm category-add-button" data-toggle="modal" data-target="#adduser">Add User</button><br><br>
          <div style="margin-top:50px;"></div>
          <div class="card shadow mb-4 category">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data of all users</h6>
            </div> -->
            
            

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Option</th>
                      <th>Name</th>
                      <th>Number</th>
                      <th>Email</th>
                      <th>Password</th>
                      <th>Verification key</th>
                      <th>Is Verified</th>
                      
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Option</th>
                      <th>Name</th>
                      <th>Number</th>
                      <th>Email</th>
                      <th>Password</th>
                      <th>Verification key</th>
                      <th>Is Verified</th>
                      
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                  // echo delete_ucategory(19);
                  foreach ($users as $key => $value) {
                  ?>
                  
                    <tr>
                      <td>
                        <div class="dropdown">
                            <button type="button" class="button-to-transpa" data-toggle="dropdown">...</button>
                            <ul class="dropdown-menu menu-drop ul_drop">
                              <li><a href="#" onclick="edit_user(<?=$value['id'];?>)" data-toggle="modal" data-target="#edituser" class="dropdown_tab">Edit</a></li>
                              <li><a href="#" onclick="delete_user(<?=$value['id'];?>)" data-toggle="modal" data-target="#delete_user" class="dropdown_tab">Delete</a></li>
                            </ul>
                        </div>
                      </td>
                      <?php 
                        $name = ($value['lname'] != '')? $value['lname'].', ' : '';
                        $name .= ($value['fname'] != '')? $value['fname'].' ' : '';
                        $name .= ($value['mname'] != '')? $value['mname'].' ' : '';
                      ?>
                      <td><?=$name?></td>
                      <td><?=$value['number'];?></td>
                      <td><?=$value['email'];?></td>
                      <td><?=$value['password'];?></td>
                      <td><?=$value['verification_key'];?></td>
                      <td><?php echo ($value['is_verified'] == '0')? 'No' : 'Yes' ;?></td>
                      
                    </tr>
                  
                  <?php } ?>
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>

<script>
  function login_show_pass() {
      var x = document.getElementById("password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
  }

  function login_show_pass2() {
      var x = document.getElementById("password2");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
  }

  function delete_user(id){
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Admin/get_user_array',
        data:{'id':id},
        success:function(data)
        {
          if(data){
              $('#delete_user #name').val(data.fname+' '+data.mname+' '+data.lname);
          }else{
            
          }
        }
    });
    $('#delete_user #id').val(id);
  }

  function edit_user(id){
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Admin/get_user_array',
        data:{'id':id},
        success:function(data)
        {
          if(data){
              $('#edit_user #fname').val(data.fname);
              $('#edit_user #mname').val(data.mname);
              $('#edit_user #lname').val(data.lname);

              $('#edit_user #number').val(data.number);
              $('#edit_user #email').val(data.email);
              $('#edit_user #old_email').val(data.email);
          }else{

          }
        }
    });

    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Admin/get_user_password',
        data:{'id':id},
        success:function(data)
        {
          if(data){
              $('#edit_user #password2').val(data);
          }else{
            
          }
        }
    });
    
    $('#edit_user #id').val(id);
  }
</script>

<!-- add User -->
  <div class="modal fade" id="adduser" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="add_user">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="fname">First Name:</label>
              <input type="text" class="form-control" id="fname" name="fname" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="mname">Middle Name:</label>
              <input type="text" class="form-control" id="mname" name="mname" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="lname">Last Name:</label>
              <input type="text" class="form-control" id="lname" name="lname" autocomplete="off" required>
            </div>

            <div class="form-group">
              <label for="number">Phone Number:</label>
              <input type="number" class="form-control" id="number" name="number" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" id="email" name="email" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="password">Password:</label>
              <input type="password" class="form-control" id="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
              <p style="color:black;"><input type="checkbox" onclick="login_show_pass()"> Show Password</p>
              <div id="message">
                <h3>Password must contain the following:</h3>
                <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
                <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
                <p id="number2" class="invalid">A <b>number</b></p>
                <p id="length" class="invalid">Minimum <b>8 characters</b></p>
              </div>
        

            </div>
            
            <div class="form-group">
              <label for="verification_key">Verification key:</label>
                <input type="text" class="form-control" id="verification_key" value="<?php echo rand(); ?>" readonly>
                <input type="hidden" id="is_verified" value="1">
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>

<!-- Edit category -->
  <div class="modal fade" id="edituser" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="edit_user">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="fname">First Name:</label>
              <input type="text" class="form-control" id="fname" name="fname" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="mname">Middle Name:</label>
              <input type="text" class="form-control" id="mname" name="mname" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="lname">Last Name:</label>
              <input type="text" class="form-control" id="lname" name="lname" autocomplete="off" required>
            </div>

            <div class="form-group">
              <label for="number">Phone Number:</label>
              <input type="number" class="form-control" id="number" name="number" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" id="email" name="email" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="password">Password:</label>
              <input type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" class="form-control" id="password2" name="password2" required>
              <p style="color:black;"><input type="checkbox" onclick="login_show_pass2()"> Show Password</p>
            </div>
            <input type="hidden" name="id" id="id">
            <input type="hidden" id="old_email" name="old_email">

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Edit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>


<!-- Delete category -->
  <div class="modal fade" id="delete_user" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="delete_user">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="name">Do you want to delete this user?</label>
              <input type="text" class="form-control" id="name" name="name" disabled="true">
              <input type="hidden" name="id" id="id">
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Delete</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>


<style>

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: "✔";
}

/* Add a red text color and an "x" when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: "✖";
}
</style>



<script>
var myInput = document.getElementById("password");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number2");
var length = document.getElementById("length");

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById("message").style.display = "none";
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
  }
  
  // Validate capital letters
  var upperCaseLetters = /[A-Z]/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove("invalid");
    capital.classList.add("valid");
  } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }
}
</script>