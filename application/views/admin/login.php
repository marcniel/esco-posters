<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(<?php echo base_url(); ?>assets/images/escoposters.png);">
					<span class="login100-form-title-1">
						Admin Login
					</span>
				</div>

				<form class="login100-form" id = "submit_cms_login">
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100">Email</span>
						<input class="input100" type="email" id="email" name="email" placeholder="Enter your email" autocomplete="off" required="">
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<div style="display: flex; align-items: center;">
							<input class="input100" type="password" name="pass" placeholder="Enter password" id="password" required>
							<span class="focus-input100"></span>
						<p style="color:black;"><i class="fa fa-eye-slash showpass" aria-hidden="true" onclick="login_show_pass();"></i></p>
						</div>
					</div>

					<div class="flex-sb-m w-full p-b-30">
						
						<div>
							<a href="#" data-toggle="modal" data-target="#forget_password" >
								Forgot Password?
							</a>
						</div>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	function login_show_pass() {
      var x = document.getElementById("password");

      if (x.type === "password") {
      	$('.showpass').removeClass('fa-eye-slash').addClass('fa-eye');
        x.type = "text";
      } else {
      	$('.showpass').removeClass('fa-eye').addClass('fa-eye-slash');
        x.type = "password";
      }
    }
</script>

<!-- add category -->
  <div class="modal fade" id="forget_password" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Forgot password?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="forgotpassword">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="email">Please put your email</label>
              <input type="email" class="form-control" id="email" name="email" autocomplete="off">
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>