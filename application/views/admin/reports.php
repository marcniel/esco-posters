          <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
          <div class="card shadow mb-4 category">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data of all users</h6>
            </div> -->
            
            

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Option</th>
                      <th>Reported By</th>
                      <th>Subject</th>
                      <th>Email</th>
                      <th>Content</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Option</th>
                      <th>Reported By</th>
                      <th>Subject</th>
                      <th>Email</th>
                      <th>Content</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                  // echo delete_ucategory(19);
                  foreach ($reports as $key => $value) {
                  ?>
                  
                    <tr>
                      <td>
                        <div class="dropdown">
                            <button type="button" class="button-to-transpa" data-toggle="dropdown">...</button>
                            <ul class="dropdown-menu menu-drop ul_drop">
                              <li><a href="#" onclick="send_email(<?=$value['report_id'];?>)" data-toggle="modal" data-target="#send_email" class="dropdown_tab">Send Email</a></li>
                            </ul>
                        </div>
                      </td>                     
                      <?php 
                        $name = ($value['lname'] != '')? $value['lname'].', ' : '';
                        $name .= ($value['fname'] != '')? $value['fname'].' ' : '';
                      ?>
                      <td><?=$name?></td>
                      <td><?=$value['subject'];?></td>
                      <td><?=$value['email'];?></td>
                      <td><?=$value['content'];?></td>
                    </tr>
                  
                  <?php } ?>
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>

<script>
  function send_email(id){
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Users/get_report_array',
        data:{'id':id},
        success:function(data)
        {
          if(data){
              $('#send_email #email').val(data.email);
              $('#send_email #subject').val(data.subject);
          }else{
            
          }
        }
    });
  }
</script>

<!-- Send Email -->
  <div class="modal fade" id="send_email" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Send Email</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="send_email">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="subject">Subject</label>
              <input type="text" class="form-control" id="subject" name="subject">
            </div>
            <div class="form-group">
              <label for="email_content">Email Content</label>
              <textarea class="form-control" id="email_content" name="email_content" required=""></textarea>
              <script>
                CKEDITOR.replace( 'email_content' );
              </script>
            </div>
            <input type="hidden" name="email" id="email">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Send</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>
