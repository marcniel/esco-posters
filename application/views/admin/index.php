<div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-4 mb-4 bottom-margin-20">

            	<div class="card border-left-primary shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                      <a href="<?php echo base_url(); ?>cms/users"><div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><u>Users</u></div></a>
	                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=check_user_count()?></div>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-user user-font"></i>
	                    </div>
	                  </div>
	                </div>
              	</div>

            </div>
            <div class="col-xl-4 col-md-4 mb-4 bottom-margin-20">

            	<div class="card border-left-primary shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                      <a href="<?php echo base_url(); ?>cms/posters"><div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><u>Posters</u></div></a>
	                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=check_poster_count()?></div>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-file user-font"></i>
	                    </div>
	                  </div>
	                </div>
              	</div>

            </div>
            <div class="col-xl-4 col-md-4 mb-4 bottom-margin-20">

            	<div class="card border-left-primary shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                      <a href="<?php echo base_url(); ?>cms/categories"><div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><u>Categories</u></div></a>
	                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=check_categories_count()?></div>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-list user-font"></i>
	                    </div>
	                  </div>
	                </div>
              	</div>

            </div>

            <!-- <div class="col-xl-3 col-md-3 mb-4">

            	<div class="card border-left-primary shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                      <a href="http://localhost/quiz/Admin/user"><div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><u>Users</u></div></a>
	                      <div class="h5 mb-0 font-weight-bold text-gray-800">count</div>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-user user-font"></i>
	                    </div>
	                  </div>
	                </div>
              	</div>

            </div> -->

  <div class="col-xl-12 col-md-12 mb-12 bottom-margin-20">
    <div class="card card-chart mb-4">
        <div class="card-header">
            <i class="fas fa-chart-area mr-1"></i>
            Posters Graph
        </div>
        <!-- <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div> -->
        <div class="card-body"><canvas id="posterChart" width="100%" height="40"></canvas></div>
    </div>
  </div>
</div>
