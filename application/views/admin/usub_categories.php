<?php include('global/category_type.php'); ?>        
    <button type="button" class="btn btn-info btn-sm category-add-button sub-category" data-toggle="modal" data-target="#addusubcategory">Add Under Sub-Category</button>
          <div style="margin-top:50px;"></div>
          <div class="card shadow mb-4 category">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data of all users</h6>
            </div> -->
            
            

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Option</th>
                      <th>Category Name</th>
                      <th>Sub Category</th>
                      <th>Under Sub Category</th>
                      
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Option</th>
                      <th>Category Name</th>
                      <th>Sub Category</th>
                      <th>Under Sub Category</th>
                      
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                  if(isset($_GET['id'])){
                    $usub_category2 = get_usub_category($_GET['id']);
                  }else{
                    $usub_category2 = $usub_category;
                  } 
                  foreach ($usub_category2 as $key => $value) {
                  ?>
                  
                    <tr>
                      <td>
                        <div class="dropdown">
                            <button type="button" class="button-to-transpa" data-toggle="dropdown">...</button>
                            <ul class="dropdown-menu menu-drop ul_drop">
                              <li><a href="#" onclick="edit_item_usub_category(<?=$value['usc_id'];?>,'<?=$value['sc_id'];?>')" data-toggle="modal" data-target="#editusubcategory" class="dropdown_tab">Edit</a></li>
                              <li><a href="#" onclick="delete_item_usub_category(<?=$value['usc_id'];?>,'<?=$value['sc_id'];?>')" data-toggle="modal" data-target="#delsubcategory" class="dropdown_tab">Delete</a></li>
                            </ul>
                        </div>
                      </td>
                      <td><?=$value['name'];?></td>
                      <td><?=$value['sname'];?></td>
                      <td><?=$value['usname'];?></td>
                      
                    </tr>
                  
                  <?php } ?>
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>

<script>
  function delete_item_usub_category(id,name){
    // $('#delete_usub_category #subcategory').val(subcategory);
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Admin/get_data',
        data:{'id':id,'table':'usub_category','field':'usname'},
        success:function(data)
        {
          if(data){
              $('#delete_usub_category #usubcategory').val(data);
          }else{
            
          }
        }
    });
    $('#delete_usub_category #subcategory').val(name);
    $('#delete_usub_category #id').val(id);
  }

  function edit_item_usub_category(id,name){
    // $('#edit_usub_category #subcategory').val(subcategory);
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Admin/get_data',
        data:{'id':id,'table':'usub_category','field':'usname'},
        success:function(data)
        {
          if(data){
              $('#edit_usub_category #usubcategory').val(data);
          }else{
            
          }
        }
    });
    $('#edit_usub_category #subcategory').val(name);
    $('#edit_usub_category #id').val(id);
  }
</script>

<!-- add category -->
  <div class="modal fade" id="addusubcategory" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add Under Sub-Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="add_usub_category">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="subcategory">Category - Sub-Category Name:</label>
              <select class="form-control" id="subcategory" name="subcategory">
                <?php 
                  foreach ($sub_category as $key => $value) {
                    if(isset($_GET['id'])){
                      if($_GET['id'] == $value['sc_id']){
                        echo '<option value="'.$value['sc_id'].'" selected>'.$value['name'].' - '.$value['sname'].'</option>';
                      }else{
                        echo '<option value="'.$value['sc_id'].'">'.$value['name'].' - '.$value['sname'].'</option>';
                      }
                    }else{
                      echo '<option value="'.$value['sc_id'].'">'.$value['name'].' - '.$value['sname'].'</option>';
                    }
                  }
                ?>
              </select>
              
            </div>

            <div class="form-group">
              <label for="usubcategory">Under Sub-Category Name:</label>
              <textarea class="form-control" id="usubcategory" name="usubcategory"></textarea>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>

<!-- Edit category -->
  <div class="modal fade" id="editusubcategory" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Under Sub Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="edit_usub_category">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="subcategory">Category - Sub-Category Name:</label>
              <select class="form-control" id="subcategory" name="subcategory">
                <?php 
                  foreach ($sub_category as $key => $value) {
                    echo '<option value="'.$value['sc_id'].'">'.$value['name'].' - '.$value['sname'].'</option>';
                  }
                ?>
              </select>
              
            </div>

            <div class="form-group">
              <label for="usubcategory">Under Sub-Category Name:</label>
              <textarea class="form-control" id="usubcategory" name="usubcategory"></textarea>
            </div>
              <input type="hidden" name="id" id="id">
        
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Edit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>


<!-- Delete category -->
  <div class="modal fade" id="delsubcategory" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Sub-Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="delete_usub_category">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="subcategory">Category - Sub-Category Name:</label>
              <select class="form-control" id="subcategory" name="subcategory">
                <?php 
                  foreach ($sub_category as $key => $value) {
                    echo '<option value="'.$value['sc_id'].'">'.$value['name'].' - '.$value['sname'].'</option>';
                  }
                ?>
              </select>
              
            </div>

            <div class="form-group">
              <label for="usubcategory">Under Sub-Category Name:</label>
              <textarea class="form-control" id="usubcategory" name="usubcategory"></textarea>
            </div>
              <input type="hidden" name="id" id="id">
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Delete</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>