<?php include('global/category_type.php'); ?>        
    <button type="button" class="btn btn-info btn-sm category-add-button sub-category" data-toggle="modal" data-target="#addcategory">Add Sub-Category</button>
          <div style="margin-top:50px;"></div>
          <div class="card shadow mb-4 category">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data of all users</h6>
            </div> -->
            
            

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Option</th>
                      <th>Category Name</th>
                      <th>Sub Category</th>
                      
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Option</th>
                      <th>Category Name</th>
                      <th>Sub Category</th>
                      
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                  if(isset($_GET['id'])){
                    $sub_category2 = get_sub_category($_GET['id']);
                  }else{
                    $sub_category2 = $sub_category;
                  } 
                  foreach ($sub_category2 as $key => $value) {
                  ?>
                  
                    <tr>
                      <td>
                        <div class="dropdown">
                            <button type="button" class="button-to-transpa" data-toggle="dropdown">...</button>
                            <ul class="dropdown-menu menu-drop ul_drop">
                              <li><a href="<?php echo base_url().'cms/usub_category?id='.$value['sc_id'] ?>" class="dropdown_tab">Open Under Sub Category</a></li>
                              <li><a href="#" onclick="edit_item_sub_category(<?=$value['sc_id'];?>,'<?=$value['cid'];?>')" data-toggle="modal" data-target="#editsubcategory" class="dropdown_tab">Edit</a></li>
                              <li><a href="#" onclick="delete_item_sub_category(<?=$value['sc_id'];?>,'<?=$value['cid'];?>')" data-toggle="modal" data-target="#delsubcategory" class="dropdown_tab">Delete</a></li>
                            </ul>
                        </div>
                      </td>
                      <td><?=$value['name'];?></td>
                      <td><?=$value['sname'];?></td>
                      
                    </tr>
                  
                  <?php } ?>
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>

<script>
  function delete_item_sub_category(id,name){
    // $('#delete_sub_category #subcategory').val(subcategory);
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Admin/get_data',
        data:{'id':id,'table':'sub_category','field':'sname'},
        success:function(data)
        {
          if(data){
              $('#delete_sub_category #subcategory').val(data);
          }else{
                      }
        }
    });
    $('#delete_sub_category #category').val(name);

    
    $('#delete_sub_category #id').val(id);
  }

  function edit_item_sub_category(id,name){
    // $('#edit_sub_category #subcategory').val(subcategory);
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Admin/get_data',
        data:{'id':id,'table':'sub_category','field':'sname'},
        success:function(data)
        {
          if(data){
              $('#edit_sub_category #subcategory').val(data);
          }else{
                      }
        }
    });
    $('#edit_sub_category #category').val(name);
    $('#edit_sub_category #id').val(id);
  }
</script>

<!-- add category -->
  <div class="modal fade" id="addcategory" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add Sub-Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="add_sub_category">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="category">Category Name:</label>
              <select class="form-control" id="category" name="category">
                <?php 
                  foreach ($category as $key => $value) {
                    if(isset($_GET['id'])){
                      if($_GET['id'] == $value['cid']){
                        echo '<option value="'.$value['cid'].'" selected>'.$value['name'].'</option>';
                      }else{
                        echo '<option value="'.$value['cid'].'">'.$value['name'].'</option>';
                      }
                    }else{
                      echo '<option value="'.$value['cid'].'">'.$value['name'].'</option>';
                    }
                  }
                ?>
              </select>
              
            </div>

            <div class="form-group">
              <label for="subcategory">Sub-Category Name:</label>
              <textarea class="form-control" id="subcategory" name="subcategory"></textarea>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>

<!-- Edit category -->
  <div class="modal fade" id="editsubcategory" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Sub Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="edit_sub_category">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="category">Category Name:</label>
              <select class="form-control" id="category" name="category">
                <?php 
                  foreach ($category as $key => $value) {
                ?>
                <option value="<?=$value['cid']?>"><?=$value['name']?></option>
                <?php
                  }
                ?>
              </select>
              
            </div>

            <div class="form-group">
              <label for="subcategory">Sub-Category Name:</label>
              <textarea class="form-control" id="subcategory" name="subcategory"></textarea>
            </div>
              <input type="hidden" name="id" id="id">
        
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Edit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>


<!-- Delete category -->
  <div class="modal fade" id="delsubcategory" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Sub-Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="delete_sub_category">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="category">Category Name:</label>
              <select class="form-control" id="category" name="category">
                <?php 
                  foreach ($category as $key => $value) {
                ?>
                <option value="<?=$value['cid']?>"><?=$value['name']?></option>
                <?php
                  }
                ?>
              </select>
              
            </div>

            <div class="form-group">
              <label for="subcategory">Sub-Category Name:</label>
              <input type="text" class="form-control" id="subcategory" name="subcategory" disabled="">
            </div>
              <input type="hidden" name="id" id="id">
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Delete</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>