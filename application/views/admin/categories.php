<?php include('global/category_type.php'); ?>    
    <button type="button" class="btn btn-info btn-sm category-add-button" data-toggle="modal" data-target="#addcategory">Add Category</button>
          <div style="margin-top:50px;"></div>
          <div class="card shadow mb-4 category">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data of all users</h6>
            </div> -->
            
            

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Option</th>
                      <th>Name</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Option</th>
                      <th>Name</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                  // echo delete_ucategory(19);
                  foreach ($category as $key => $value) {
                  ?>
                  
                    <tr>
                      <td>
                        <div class="dropdown">
                            <button type="button" class="button-to-transpa" data-toggle="dropdown">...</button>
                            <ul class="dropdown-menu menu-drop ul_drop">
                              <li><a href="<?php echo base_url(); ?>cms/sub_category?id=<?=$value['cid'];?>" class="dropdown_tab">Open sub-category</a></li>
                              <li><a href="#" onclick="edit_item(<?=$value['cid'];?>)" data-toggle="modal" data-target="#editcategory" class="dropdown_tab">Edit</a></li>
                              <li><a href="#" onclick="delete_item(<?=$value['cid'];?>)" data-toggle="modal" data-target="#delcategory" class="dropdown_tab">Delete</a></li>
                            </ul>
                        </div>
                      </td>
                      <td><?=$value['name'];?></td>
                      
                    </tr>
                  
                  <?php } ?>
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>

<script>
  function delete_item(id){
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Admin/get_data',
        data:{'id':id,'table':'category','field':'name'},
        success:function(data)
        {
          if(data){
              $('#delete_category #category').val(data);
          }else{
            
          }
        }
    });
    $('#delete_category #id').val(id);
  }

  function edit_item(id){
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:base_url+'Admin/get_data',
        data:{'id':id,'table':'category','field':'name'},
        success:function(data)
        {
          if(data){
              $('#edit_category #category').val(data);
          }else{
            
          }
        }
    });
    
    $('#edit_category #id').val(id);
  }
</script>

<!-- add category -->
  <div class="modal fade" id="addcategory" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="add_category">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="category">Category Name:</label>
              <textarea class="form-control" id="category" name="category"></textarea>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>

<!-- Edit category -->
  <div class="modal fade" id="editcategory" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="edit_category">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="category">Category Name</label>
              <textarea class="form-control" id="category" name="category"></textarea>
              <input type="hidden" name="id" id="id">
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Edit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>


<!-- Delete category -->
  <div class="modal fade" id="delcategory" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="delete_category">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="category">Do you want to delete this category?</label>
              <input type="text" class="form-control" id="category" name="category" disabled="true">
              <input type="hidden" name="id" id="id">
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Delete</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>