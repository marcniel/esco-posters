    <a href="<?php echo base_url().'cms/add_poster' ?>" class="btn btn-info btn-sm category-add-button" target="_blank">Add Poster</a><br><br>
          <div style="margin-top:50px;"></div>
          <div class="card shadow mb-4 category">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data of all users</h6>
            </div> -->
            
            

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Option</th>
                      <th>Posted by</th>
                      <th>Poster Title</th>
                      <th>Poster Content</th>
                      <th>Category</th>
                      <th>Sub Category</th>
                      <th>Under Sub Category</th>
                      <th>Is Downloadable</th>
                      
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Option</th>
                      <th>Posted by</th>
                      <th>Poster Title</th>
                      <th>Poster Content</th>
                      <th>Category</th>
                      <th>Sub Category</th>
                      <th>Under Sub Category</th>
                      <th>Is Downloadable</th>
                      
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                  foreach ($posters as $key => $value) {
                  ?>
                  
                    <tr>
                      <td>
                        <div class="dropdown">
                            <button type="button" class="button-to-transpa" data-toggle="dropdown">...</button>
                            <ul class="dropdown-menu menu-drop ul_drop">
                              <li><a href="<?php echo base_url().'uploads/'.$value['poster_file'] ?>" target="_blank" class="dropdown_tab">View File</a></li>
                              <li><a href="<?php echo base_url().'cms/edit_poster/'.$value['poster_id'] ?>" target="_blank" class="dropdown_tab">Edit</a></li>
                              <li><a href="#" onclick="delete_poster(<?=$value['poster_id'];?>)" data-toggle="modal" data-target="#delete_poster" class="dropdown_tab">Delete</a></li>
                            </ul>
                        </div>
                      </td>
                      <td><?=$value['poster_by']?></td>
                      <td><?=$value['poster_name'];?></td>
                      <td><?=$value['poster_content'];?></td>
                      <td><?=$value['name'];?></td>
                      <td><?=$value['sname'];?></td>
                      <td><?=$value['usname'];?></td>
                      <td><?php echo ($value['is_downloadable'] == '0')? 'No' : 'Yes' ;?></td>
                      
                    </tr>
                  
                  <?php } ?>
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>
<script>
  function delete_poster(id){
    $('#delete_poster #id').val(id);
  }
</script>
<!-- Delete category -->
  <div class="modal fade" id="delete_poster" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Poster</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
      <form id="delete_poster">
        <div class="modal-body">
          
            <div class="form-group">
              <label for="name">Do you want to delete this poster?</label>
              <input type="hidden" name="id" id="id">
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Delete</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>