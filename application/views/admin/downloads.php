          <div class="card shadow mb-4 category">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data of all users</h6>
            </div> -->
            
            

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Poster Download By</th>
                      <th>Poster Downloaded</th>
                      <th>Date</th>
                      
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Poster Download By</th>
                      <th>Poster Downloaded</th>
                      <th>Date</th>
                      
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                  // echo delete_ucategory(19);
                  foreach ($downloads as $key => $value) {
                  ?>
                  
                    <tr>
                      
                      <?php 
                        $name = ($value['lname'] != '')? $value['lname'].', ' : '';
                        $name .= ($value['fname'] != '')? $value['fname'].' ' : '';
                        $name .= ($value['mname'] != '')? $value['mname'].' ' : '';
                      ?>
                      <td><?=$name?></td>
                      <td><?=$value['poster_name'];?></td>
                      <td><?=timeAgo($value['date_get']);?></td>
                      
                    </tr>
                  
                  <?php } ?>
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>

