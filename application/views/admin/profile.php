          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Profile</h1>
          </div>
          <center class="container_profile">
          
            <img src="<?php echo get_current_admin_image(); ?>" onerror="this.src='<?php echo image_onerror() ?>';" id="image_view" class="img-profile rounded-circle image" height="200" width="200" onclick="upload_image()">
            <div class="overlay">
              <div class="text"><a style="color:black;background: white;" onclick="upload_image()">Click to Change Image</a></div>
            </div>
          </center>
          <input type="file" name="image" style="display: none;" id="clicked_image" accept="image/x-png,image/gif,image/jpeg">
          <form id="update_profile">
          	<div class="row">
	    		<div class="col-xs-12 col-xm-12 col-md-4 col-lg-4">
				  <div class="form-group">
				    <label for="fname">First Name:</label>
				    <input type="text" class="form-control" value = "<?php echo get_admin('fname'); ?>" name="fname" id="fname" autocomplete="off" required>
				  </div>
				</div>
				<div class="col-xs-12 col-xm-12 col-md-4 col-lg-4">
				  <div class="form-group">
				    <label for="mname">Middle Name:</label>
				    <input type="text" class="form-control"  value = "<?php echo get_admin('mname'); ?>" name="mname" id="mname" autocomplete="off">
				  </div>
				</div>
        <div class="col-xs-12 col-xm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <label for="lname">Last Name:</label>
            <input type="text" class="form-control" value = "<?php echo get_admin('lname'); ?>" name="lname" id="lname" autocomplete="off" required>
          </div>
        </div>
        
			</div>

			<div class="row">
	    		<div class="col-xs-12 col-xm-12 col-md-6 col-lg-6">
				  <div class="form-group">
				    <label for="email">Email:</label>
				    <input type="text" class="form-control" value = "<?php echo get_admin('email'); ?>" name="email" id="email" autocomplete="off" required>
				  </div>
				</div>
				<div class="col-xs-12 col-xm-12 col-md-6 col-lg-6">
				  <div class="form-group">
				    <label for="password">Password:</label>
				    <input type="password" class="form-control" value = "<?php echo base64_decode(get_admin('password')); ?>" name="password" id="password" autocomplete="off" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
            <p style="color:black;"><input type="checkbox" onclick="login_show_pass()"> Show Password</p>
				  </div>
				</div>
			</div>
			  <button type="submit" class="btn btn-success" style="float:right;">Submit</button>
			</form>


<script>
	function login_show_pass() {
      var x = document.getElementById("password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }

    
        function upload_image(){
          $('#clicked_image').click();
        }

        $("#clicked_image").change(function() {
               if(app.getFileExtension($(this).val()) == 'png' ||
                app.getFileExtension($(this).val()) == 'gif' ||
                app.getFileExtension($(this).val()) == 'pjp' ||
                app.getFileExtension($(this).val()) == 'jpg' ||
                app.getFileExtension($(this).val()) == 'pjpeg' ||
                app.getFileExtension($(this).val()) == 'jpeg' ||
                app.getFileExtension($(this).val()) == 'jfif'
                ){
                readURL(this);
              }else{
              app.alert('Uploaded file error','Please upload file atleast png,gif,pjp,jpg,pjpeg,jpeg or jfif');
              }
              
        });

        function readURL(input) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
              // $('#image_view').attr('src', e.target.result);
              $.ajax({
                type:'POST',
                dataType:'JSON',
                url:base_url+'cms/upload_image',
                data:{'upload_image':e.target.result},
                success:function(data)
                {
                  if(data == 1){
                    // alert(data.admin);
                    app.alert_redirection('Success','Image Updated',base_url+'cms/profile');
                    // setTimeout(function(){location.reload()},3000)
                }else{
                  app.alert('Error',data);
                }
                }
              });
            }
            
            reader.readAsDataURL(input.files[0]);
          }
        }

</script>