<a href="<?php echo base_url() ?>cms/categories" class="btn btn-<?php echo ($category_type == 'category')? 'info' : 'default'; ?> btn-sm category_type_button">Categories</a>
<a href="<?php echo base_url() ?>cms/sub_category" class="btn btn-<?php echo ($category_type == 'subcategory')? 'info' : 'default'; ?> btn-sm subcategory_type_button">Sub-Categories</a>
<a href="<?php echo base_url() ?>cms/usub_category" class="btn btn-<?php echo ($category_type == 'usubcategory')? 'info' : 'default'; ?> btn-sm usubcategory_type_button">Under Sub-Categories</a>
<br><br>