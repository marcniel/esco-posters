<div class="container-fluid">
<div class="row">
	<div class="col-lg-2 col-md-3 col-xs-3 col-3 nopadding col1">
	<div class="" id="mySidebar">
		<div class="img bg-wrap text-center py-4" style="background-image: url(<?php echo base_url();?>assets/images/escoposters.png);">
  			<a href="<?=base_url().'cms/profile'?>"><div class="user-logo">
        <?php echo (get_current_admin_image() != '')? '<div class="img img_data" style="background-image: url('.get_current_admin_image().');"></div>' : '<div class="img" style="background-image: url('.image_onerror().');"></div>' ;?>
  				
  				<h3><?php echo (get_admin('fname') != '')? get_admin('fname').' ' : '';echo (get_admin('mname') != '')? get_admin('mname').'. ' : '';echo (get_admin('lname') != '')? get_admin('lname') : ''; ?></h3>
  			</div></a>
  		</div>
	  <!-- <button onclick="w3_close()" class="close">&times; Close</button> -->
	  <a href="<?=base_url().'cms'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'dashboard')?'active':'';?>"><i class="fa fa-dashboard"></i> Dashboard</a>
	  <a href="<?=base_url().'cms/posters'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'posters')?'active':'';?>"><i class="fa fa-file"></i> Posters</a>
	  <a href="<?=base_url().'cms/categories'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'categories')?'active':'';?>"><i class="fa fa-list"></i> Posters Categories</a>
	  <a href="<?=base_url().'cms/downloads'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'downloads')?'active':'';?>"><i class="fa fa-download"></i> Poster Downloads</a>
	  <a href="<?=base_url().'cms/reports'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'reports')?'active':'';?>"><i class="fa fa-bug"></i> Feedbacks</a>
	  <a href="<?=base_url().'cms/users'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'users')?'active':'';?>"><i class="fa fa-user"></i> Users</a>
	  <a href="<?=base_url().'cms/profile'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'profile')?'active':'';?>"><i class="fa fa-id-card"></i> Profile</a>
	  <a href="<?=base_url().'cms/logout'?>" class="sidebar-b-bar-item sidebar-b-button"><i class="fa fa-sign-out"></i> Logout</a>
	</div>

	<!-- Page Content -->
	  <!-- <button class="sidebar-button" onclick="open_nav()">☰</button> -->
	 </div>
	 
	<!-- <script>
		function open_nav() {
	  document.getElementById("mySidebar").style.display = "block";
	}

	function w3_close() {
	  document.getElementById("mySidebar").style.display = "none";
	}
	</script> -->
	<div class="margin-top-10"></div>
	<div class="col-lg-10 col-md-9 col-xs-9 col-9 <?php echo ($page != 'categories' && $page != 'users' && $page != 'posters' && $page != 'downloads' && $page != 'reports')? 'wordbreak' : ''; ?> col2 container_width">