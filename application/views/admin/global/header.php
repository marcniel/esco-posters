<html>
  <head>
    <title><?php echo $title ?></title>
    <script>
      var base_url = "<?php echo base_url() ?>";
    </script>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/downloaded/bootstrap.min.css">
    <script src="<?php echo base_url(); ?>assets/plugin/downloaded/3.5.1jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugin/downloaded/bootstrap.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/app.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/graph.css">
    <?php echo ($page == 'login')? '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugin/login/css/util.css">
  <link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugin/login/css/main.css">' : '' ; ?>

  <!-- for jquery -->
  <script src="<?php echo base_url(); ?>assets/plugin/downloaded/171jquery.min.js" type="text/javascript"></script>
  <link href="<?php echo base_url(); ?>assets/plugin/downloaded/familygoogleapicss.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/downloaded/jquery-confirm.min.css">
    <script src="<?php echo base_url(); ?>assets/plugin/downloaded/jquery-confirm.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/app.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/sb/vendor/chart.js/Chart.js"></script>
    <?php
  if($page == 'categories' || $page == 'users' || $page == 'posters' || $page == 'downloads' || $page == 'reports'){
    echo '<link href="'.base_url().'/assets/sb/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">';
  }
  ?>
  <!-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> -->
  </head>
  <body>
    <div class="header">
      <div class="navbar-brand">
        <a href="<?=base_url().'cms'?>"><img src="<?php echo base_url(); ?>assets/images/esco-logo.png" alt="Esco" class="img-responsive img-ie"></a>

        <ul class="navbar-right">
        <li class="dropdown headnav <?php echo (admin_session_val())? '' : 'nodisplay'; ?>">
          <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo (get_admin('fname') != '')? get_admin('fname').' ' : '';echo (get_admin('mname') != '')? get_admin('mname').'. ' : '';echo (get_admin('lname') != '')? get_admin('lname') : ''; ?> <span class="caret"></span></a> -->
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">≡</a>
        <ul class="dropdown-menu droppingdown">
          <li><a href="<?=base_url().'cms'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'dashboard')?'active':'';?>">Dashboard</a></li>
          <li><a href="<?=base_url().'cms/posters'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'posters')?'active':'';?>">Posters</a></li>
          <li><a href="<?=base_url().'cms/categories'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'categories')?'active':'';?>">Posters Categories</a></li>
          <li><a href="<?=base_url().'cms/downloads'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'downloads')?'active':'';?>">Poster Downloads history</a></li>
          <li><a href="<?=base_url().'cms/reports'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'reports')?'active':'';?>">Feedbacks</a></li>
          <li><a href="<?=base_url().'cms/users'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'users')?'active':'';?>">Users</a></li>
          <li><a href="<?=base_url().'cms/profile'?>" class="sidebar-b-bar-item sidebar-b-button <?php echo ($page == 'profile')?'active':'';?>">Profile</a></li>
          <li><a href="<?=base_url().'cms/logout'?>" class="sidebar-b-bar-item sidebar-b-button">Logout</a></li>
        </ul>
        </li>
      </ul>

      </div>

    </div>
    <script src="http://malsup.github.com/jquery.form.js"></script>
