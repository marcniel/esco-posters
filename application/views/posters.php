<div class="home" id="search_container">
	<div class="row">
		<button class="btn btn-info" id="open_nav" onclick="open_nav()">☰</button>
		<div class="col-xs-1 col-sm-1 col-md-3 col-lg-2">
			<?php include('global/poster_nav.php'); ?>
		</div>










		<div class="col-xs-11 col-sm-11 col-md-9 col-lg-10">
			<div class="row" style="margin:10px">
				<form action = "<?php echo base_url().'posters' ?>" method="GET" class="search_poster">
						<input type="text" id="suggest_item" value="<?php echo ((isset($_GET['q']))? $_GET['q'] : ''); ?>" name = "q" placeholder="Search poster name" class="form-control search-textbox">
					<button><i class="fa fa-search" aria-hidden="true"></i></button>
				</form>

				
			<?php
			foreach ($posters_list as $key => $value) {
			$file_type = pathinfo($value['poster_file'], PATHINFO_EXTENSION);
			if($file_type != 'pdf' && $file_type != 'PDF'){
				echo '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 single-poster" style="padding:20px;">';
				echo '<div class="single-poster-container">';
					echo '<a class="hoverat" href = "'.base_url().'uploading/'.$value['poster_id'].'" >';
						if(file_exists('uploads/'.$value['poster_file'])){
							echo '<div class="imgdiv2" style="background-image: url('.base_url().'uploads/'.$value['poster_file'].');">';
						}else{
							echo '<div class="imgdiv2" style="background-image: url('.base_url().'assets/images/no_image.jpg);">';
						}
					echo '<button class="btn download-button"><p class="pull-right">Open</p>'.
							'<p class="pull-left"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;'.poster_view_count($value['poster_id']);
							if($value['is_downloadable'] == '1'){
								echo '&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-download" aria-hidden="true"></i>&nbsp;'.poster_download_count($value['poster_id']);
							}
							echo '</p>'.
							'</button>';
					echo '</div>';
					echo '</a>';


					echo '<div class="btn download-button2">'.
					'<h4 class="pull-left">'.$value['poster_name'].'</h4>';
					if($value['is_downloadable'] == '1'){
						echo '<a href = "'.base_url().'uploads/'.$value['poster_file'].'" download = "'.$value['poster_name'].'.'.$file_type.'" class="btn btn-success float-right-download"><i class="fa fa-download" aria-hidden="true"></i></a>';
					}
					echo '</div>';



				echo '</div>';
				echo '</div>';
			}else{
				echo '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 single-poster" style="padding:20px;">';
				echo '<div class="single-poster-container">';
					echo '<a class="hoverat" href = "'.base_url().'uploading/'.$value['poster_id'].'" >';
						if(file_exists('uploads/'.$value['poster_file_thumb']) && $value['poster_file_thumb'] != ''){
							echo '<div class="imgdiv2" style="background-image: url('.base_url().'uploads/'.$value['poster_file_thumb'].');">';
						}else{
							echo '<div class="imgdiv2" style="background-image: url('.base_url().'assets/images/pdf.png);">';
						}

					echo '<button class="btn download-button"><p class="pull-right">Open</p>'.
							'<p class="pull-left"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;'.poster_view_count($value['poster_id']);
							if($value['is_downloadable'] == '1'){
								echo '&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-download" aria-hidden="true"></i>&nbsp;'.poster_download_count($value['poster_id']);
							}
							echo '</p>'.
							'</button>';
					echo '</div>';
					echo '</a>';


					echo '<div class="btn download-button2">'.
					'<h4 class="pull-left">'.$value['poster_name'].'</h4>';
					if($value['is_downloadable'] == '1'){
						echo '<a href = "'.base_url().'uploads/'.$value['poster_file'].'" download = "'.$value['poster_name'].'.'.$file_type.'" class="btn btn-success float-right-download"><i class="fa fa-download" aria-hidden="true"></i></a>';
					}
					echo '</div>';


				echo '</div>';
				echo '</div>';
			}
			?>
			<?php }
				echo "<div class = 'pagination_div'>";
					foreach ($page_links as $link){
						echo $link;
					}
				echo "</div>";
			?>
			</div>
		</div>
</div>
